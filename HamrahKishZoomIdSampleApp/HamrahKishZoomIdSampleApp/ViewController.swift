//
//  ViewController.swift
//  HamrahKishZoomIdSampleApp
//
//  Created by Mojtaba Mirzadeh on 10/9/1399 AP.
//  Copyright © 1399 AP Hamrahkish. All rights reserved.
//

import UIKit
import Pods_HamrahKishZoomIdSampleApp

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func callFrameworkPressed(_ sender: Any) {
        let manager = SDKManager()
        let vc = manager.startViewController(licenceTxt: "BEQ806uX6f3wBwdV2TOHPclEsWDAGHrj4B7LC34kpPFQZ6jVK6n27h5tyX05RFk6H9EGDfaNvzWwBcL/KszTJg==")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

