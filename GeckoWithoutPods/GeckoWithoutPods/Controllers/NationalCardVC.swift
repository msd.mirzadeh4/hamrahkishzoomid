//
//  NationalCardVC.swift
//  GeckoFramework
//
//  Created by Mojtaba Mirzadeh on 8/25/1399 AP.
//  Copyright © 1399 AP Hamrahkish. All rights reserved.
//

import UIKit
import ZoomAuthentication
import VisionKit
//import 
//import NVActivityIndicatorView

class NationalCardVC: UIViewController, NVActivityIndicatorViewable, UITextFieldDelegate, ProcessingDelegate, URLSessionDelegate {
    
    //TextField
    @IBOutlet weak var nationalCardSerialTxtField: UITextField!
    @IBOutlet weak var birthdayTxtField: UITextField!
    
    //Views
//    @IBOutlet weak var frontMeliicardBtn: UIButton!
//    @IBOutlet weak var rearMellicardBtn: UIButton!
    @IBOutlet weak var birthdayView: UIView!
    @IBOutlet weak var serialNumView: UIView!
    
//    //Constraits
//    @IBOutlet weak var frontMellicardConst: NSLayoutConstraint!
//    @IBOutlet weak var rearMellicardConst: NSLayoutConstraint!
    
    var responseContactValidation: ResponseContactValidation!
    var responsePostInquiry = ResponsePostInquiry()
    var responseIndividualAndImage = ResponseIndividualAndImage()
    var latestZoomSessionResult: ZoomSessionResult!
    
    private var btnSenderTag = 0
    var isSuccess = false
    var requestInProgress: Bool = false
    
    var phoneNumber = ""
    
    private var garegoriBirthday: String = "1991-01-02"
    
//    let datePicker = DatePickerDialog(textColor: .black, buttonColor: .white, font: UIFont(name: "IRANSans-Bold", size: 13)! ,buttonBackgroundColor: #colorLiteral(red: 0.07843137255, green: 0.337254902, blue: 0.6588235294, alpha: 1), showCancelButton: false)
//    let formatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        //Scanner View Settings
//        self.frontMellicardConst.constant = (164 * self.frontMeliicardBtn.frame.width) / 284.44
//        self.rearMellicardConst.constant = (164 * self.rearMellicardBtn.frame.width) / 284.44
//        self.frontMeliicardBtn.contentMode = UIView.ContentMode.scaleToFill
//        self.rearMellicardBtn.contentMode = UIView.ContentMode.scaleToFill
        
        self.birthdayTxtField.delegate = self
        
        initVC()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.nationalCardSerialTxtField.resignFirstResponder()
        return true
    }
    
    func zoomVC() {
        self.startAnimating()
        
        ThemeHelpers.setAppTheme(theme: "FaceTec Theme")
        
        
        xprint(ZoomGlobalState.faceIDLicenceText)
        
        Zoom.sdk.initialize(licenseText: ZoomGlobalState.faceIDLicenceText,
                            licenseKeyIdentifier: ZoomGlobalState.DeviceLicenseKeyIdentifier,
                            faceMapEncryptionKey: ZoomGlobalState.PublicFaceMapEncryptionKey,
                            completion: { initializationSuccessful in
                                
                                if(initializationSuccessful) {
                                    let _ = LivenessCheckProcessor(delegate: self, fromVC: self)
                                    self.stopAnimating()
                                } else {
                                    self.stopAnimating()
                                    self.view.makeToast("\(Zoom.sdk.description(for: Zoom.sdk.getStatus()))", duration: 4, position: .center)
                                    xprint("\(Zoom.sdk.description(for: Zoom.sdk.getStatus()))")
                                }
        })
        
    }
    
    func onProcessingComplete(isSuccess: Bool, zoomSessionResult: ZoomSessionResult?) {
        
        latestZoomSessionResult = zoomSessionResult
        faceMatch(sourceFaceMap: (zoomSessionResult?.faceMetrics?.faceMapBase64)!, auditTrailImage: zoomSessionResult!.faceMetrics!.auditTrailCompressedBase64![0], lowQualityAuditTrailImage: zoomSessionResult!.faceMetrics!.lowQualityAuditTrailCompressedBase64![0] , sessionId: zoomSessionResult!.sessionId)
        
    }
    
    func onProcessingComplete(isSuccess: Bool, zoomSessionResult: ZoomSessionResult?, zoomIDScanResult: ZoomIDScanResult?) {
        
    }
    
    func onSessionTokenError() {
        
    }
    
    func initVC() {
        // Add Image And Font And Title For NavigationBar
//        let attributes = [NSAttributedString.Key.font: UIFont(name: "IRANSans-Bold", size: 20)!]
        self.navigationController?.navigationBar.backgroundColor = UIColor(hexString: "#1456A8")
//        UINavigationBar.appearance().titleTextAttributes = attributes
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: nil)
        navigationItem.backBarButtonItem = backButton
        self.navigationItem.title = ""
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        hideKeyboardWhenTappedAround()
    }
    
//    func datePickerTapped() {
//        let calendar = Calendar(identifier: .gregorian)
//
//        let currentDate = Date()
//        var components = DateComponents()
//        components.calendar = calendar
//        components.month = 12
//        components.year = -75
//        let minDate = calendar.date(byAdding: components, to: currentDate)!
//
//        self.datePicker.show("تاریخ تولد", doneButtonTitle: "تایید", cancelButtonTitle: "انصراف", defaultDate: currentDate, minimumDate: minDate, maximumDate: currentDate, datePickerMode: .date) { (date) in
//
//            if let dt = date {
//                let formatterEN = self.formatter
//                formatterEN.calendar = NSCalendar(identifier: NSCalendar.Identifier.gregorian)! as Calendar
//                formatterEN.locale = NSLocale(localeIdentifier: "en") as Locale
//                formatterEN.dateFormat = "yyyy-MM-dd"
//                self.garegoriBirthday = formatterEN.string(from: dt)
//
//                let formatterIR = self.formatter
//                formatterIR.calendar = NSCalendar(identifier: NSCalendar.Identifier.persian)! as Calendar
//                formatterIR.locale = NSLocale(localeIdentifier: "fa_IR") as Locale
//                formatterIR.dateFormat = "yyyy/MM/dd"
////                User.getUserInstance().setbirthday(birthday: formatterIR.string(from: dt))
//                self.birthdayTxtField.text = formatterIR.string(from: dt)
//            }
//        }
//    }
    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//
//        if textField == self.birthdayTxtField {
//            datePickerTapped()
//            return false
//        }
//
//        return true
//    }
    
    func scanDocument() {
        if #available(iOS 13.0, *) {
            let scanVC = VNDocumentCameraViewController()
            scanVC.delegate = self
            present(scanVC, animated: true)
        } else {
            // Fallback on earlier versions
        }
    }
    
    //Call Webservice
    func individualAndImage() {
        self.startAnimating()
        
        let birthDateChangeFormat = Helper.shared.dateFormat(inputStr: self.birthdayTxtField.text!, inputF: "yyyy/MM/dd", outputF: "yyyy-MM-dd", inputCalendar: Calendar(identifier: .persian), outputCalendar: Calendar(identifier: .gregorian))
        
        Webservic.sheared.individualAndImage(serialNumber: self.nationalCardSerialTxtField.text!, birthDate: birthDateChangeFormat) { (response, err) in
            if response == nil {
                self.stopAnimating()
                self.view.makeToast("مشکلی در اتصال به شبکه پیش آمده لطفا بعدا امتحان کنید.", duration: 2, position: .center)
            } else if response?.getStatusCode() != 200 {
                self.stopAnimating()
                xprint("\(response?.getStatusCode())" + "\n" +  "\(response?.getMessage())")
                self.view.makeToast(response?.getMessage(), duration: 2, position: .center)
            } else {
                self.responseIndividualAndImage = response!
                self.stopAnimating()
                self.getLicence()
                //                self.zoomVC()
            }
        }
    }
    
    //Call Webservice
    func faceMatch(sourceFaceMap: String, auditTrailImage: String, lowQualityAuditTrailImage: String, sessionId: String) {
        self.startAnimating()
        Webservic.sheared.faceMatch(sourceFaceMap: sourceFaceMap, auditTrailImage: auditTrailImage, lowQualityAuditTrailImage: lowQualityAuditTrailImage, sessionId: sessionId) { (response, err) in
            if response == nil {
                self.stopAnimating()
                self.view.makeToast("مشکلی در اتصال به شبکه پیش آمده لطفا بعدا امتحان کنید.", duration: 2, position: .center)
            } else {
                
//                guard let vc = UIStoryboard(name:"Vas", bundle:nil).instantiateViewController(withIdentifier: "GetSign") as? GetSignVC else {
//                    xprint("Could not instantiate view controller with identifier of type SecondViewController")
//                    return
//                }
                
//                vc.responseIndividualAndImage = self.responseIndividualAndImage
                
                self.stopAnimating()
                
                self.performSegue(withIdentifier: "GoFinal", sender: self)
                
//                self.navigationController?.pushViewController(vc, animated:true)
                
            }
        }
    }
    
    //Call Webservice
    func getLicence() {
        self.startAnimating()
        Webservic.sheared.getLicenceText { (response, err) in
            if response == nil {
                self.stopAnimating()
                self.view.makeToast("مشکلی در اتصال به شبکه پیش آمده لطفا بعدا امتحان کنید.", duration: 2, position: .center)
            } else {
                self.stopAnimating()
                self.zoomVC()
            }
        }
    }
    
    func startAnimating(){
        let size = CGSize(width: 70, height: 70)
        let font = UIFont(name: "IRANSans-Light", size: 15)
        startAnimating(size, message: "در حال بارگذاری...", messageFont: font, type: NVActivityIndicatorType.ballClipRotateMultiple, color: #colorLiteral(red: 0.07843137255, green: 0.337254902, blue: 0.6588235294, alpha: 1),
                       displayTimeThreshold: 4, minimumDisplayTime: 2, textColor: UIColor.white)
    }
    
    @IBAction func frontMelliCardBtnPressed(_ sender: UIButton) {
        self.btnSenderTag = sender.tag
        scanDocument()
    }
    
    @IBAction func rearMelliCardBtnPressed(_ sender: UIButton) {
        self.btnSenderTag = sender.tag
        scanDocument()
    }
    
    @IBAction func confirmBtnPressed(_ sender: Any) {
//        if self.frontMeliicardBtn.currentImage!.isEqual(UIImage(named: "qr-code-scan")) == false {
//            if self.rearMellicardBtn.currentImage!.isEqual(UIImage(named: "qr-code-scan")) == false {
                if self.nationalCardSerialTxtField.text != nil {
                    if self.birthdayTxtField.text != nil {
                        self.individualAndImage()
                    } else {
                        self.view.makeToast("تاریخ تولد خود را بررسی کنید", duration: 2, position: .center)
                    }
                } else {
                    self.view.makeToast("سریال پشت کارت ملی را بررسی کنید", duration: 2, position: .center)
                }
            }
//    else {
//                self.view.makeToast("تصویر پشت کارت ملی را اسکن کنید", duration: 2, position: .center)
//            }
//        }
//else {
//            self.view.makeToast("تصویر روی کارت ملی را اسکن کنید", duration: 2, position: .center)
//        }
//    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "GoFinal") {
            let GSVC = segue.destination as? CSRVC
            GSVC?.responseIndividualAndImage = self.responseIndividualAndImage
        }
    }
    
}

extension NationalCardVC: VNDocumentCameraViewControllerDelegate {
    @available(iOS 13.0, *)
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
        guard scan.pageCount >= 1 else {
            controller.dismiss(animated: true)
            return
        }
        
//        if btnSenderTag == 1 {
//            self.frontMeliicardBtn.contentMode = .scaleAspectFit
//            self.frontMeliicardBtn.setImage(scan.imageOfPage(at: 0), for: .normal)
//        } else if btnSenderTag == 2 {
//            self.rearMellicardBtn.contentMode = .scaleAspectFit
//            self.rearMellicardBtn.setImage(scan.imageOfPage(at: 0), for: .normal)
//        }
        
        // Here will be the code for text recognition
        
        controller.dismiss(animated: true)
    }
    
    @available(iOS 13.0, *)
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFailWithError error: Error) {
        //Handle properly error
        controller.dismiss(animated: true)
    }
    
    //cansel
    @available(iOS 13.0, *)
    func documentCameraViewControllerDidCancel(_ controller: VNDocumentCameraViewController) {
        controller.dismiss(animated: true)
    }
}
