//
//  CSRVC.swift
//  GeckoFramework
//
//  Created by Mojtaba Mirzadeh on 8/26/1399 AP.
//  Copyright © 1399 AP Hamrahkish. All rights reserved.
//

import UIKit

class CSRVC: UIViewController, NVActivityIndicatorViewable, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var englishFirstName: UITextField!
    @IBOutlet weak var englishLastName: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    
    @IBOutlet weak var stateCodeTxt: UIButton!
    @IBOutlet weak var cityCodeTxt: UIButton!
    
    @IBOutlet weak var addressTxt: UITextField!
    @IBOutlet weak var postalCodeTxt: UITextField!
    @IBOutlet weak var jobTxt: UITextField!
    @IBOutlet weak var homeNumTxt: UITextField!
    @IBOutlet weak var checkBoxBtn: UIButton!
    @IBOutlet weak var confirmBtn: UIButton!
        
    @IBOutlet weak var stateCityTableView: UITableView!
        
    var responseIndividualAndImage = ResponseIndividualAndImage()
    var responsePostSignCSR = ResponsePostSignCSR()
    var stateCode = [ResponseStateCode]()
    var cityCode = [ResponseCityCode]()
    
    var phoneNumber = ""
    
    var stateName = [String]()
    var cityName = [String]()
    var tableViewCount = 0
    var BtnTag = 0
    
    //Dic
    var stateNameDic : [String : Int] = [:]
    var cityNameDic : [String : Int] = [:]
    
    var gotStateCode = 0
    var gotCityCode = 0
    
    //CSR
    let tagPrivate = "com.csr.private.rsa"
    let tagPublic = "com.csr.public.rsa"
    let tagtraceID = "com.csr.traceid.rsa"
    //    let keyAlgorithm = KeyAlgorithm.rsa(signatureType: .sha256)
    let keyAlgorithm = KeyAlgorithm.ec(signatureType: .sha256)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Clear out App Keychain (This Code Clean Keychains and let to csr to make a new ...) {
        var query: [String:AnyObject] = [String(kSecClass): kSecClassKey]
        SecItemDelete(query as CFDictionary)

        query = [String(kSecClass): kSecClassKey]
        SecItemDelete(query as CFDictionary)

        query = [String(kSecClass): kSecClassCertificate]
        SecItemDelete(query as CFDictionary)

        query = [String(kSecClass): kSecClassIdentity]
        SecItemDelete(query as CFDictionary)

        // } End Clear out App Keychain
                
        //        self.checkBoxBtn.isSelected = false
        //        self.checkBoxBtn.setImage(UIImage(named: "deselctBox"), for: .normal)
        self.confirmBtn.isEnabled = false
        self.confirmBtn.backgroundColor = #colorLiteral(red: 0.7607843137, green: 0.7607843137, blue: 0.7607843137, alpha: 1)
        
        initVC()
        getStateCode()
        self.stateCityTableView.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.stateCityTableView.dataSource = self
        self.stateCityTableView.delegate = self
        
        self.stateCityTableView.layer.masksToBounds = true
        self.stateCityTableView.layer.borderColor = #colorLiteral(red: 0.08122608811, green: 0.4230399728, blue: 0.7173703313, alpha: 1)
        self.stateCityTableView.layer.borderWidth = 2.0
        
    }
    
    //TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        xprint(self.stateName.count)
        xprint(self.cityName.count)
        
        if self.BtnTag == 0 {
            self.tableViewCount = self.stateName.count
        } else if self.BtnTag == 1 {
            self.tableViewCount = self.cityName.count
        }
        return self.tableViewCount
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = self.stateCityTableView.dequeueReusableCell(withIdentifier: "TableViewCell") as! StateTableViewCell
        if self.BtnTag == 0 {
            
            xprint(self.stateName[indexPath.row])
            xprint(self.stateName.count)

            cell.textLabel?.text = self.stateName[indexPath.row]
        } else if self.BtnTag == 1 {
            cell.textLabel?.text = self.cityName[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.BtnTag == 0 {
            
            let currentCell = self.stateName[indexPath.row]
            xprint(currentCell)
            xprint(self.stateNameDic[currentCell])
            
            self.gotStateCode = self.stateNameDic[currentCell]!
            
            xprint(self.gotStateCode)
            
            self.getCityCode(URLState: self.gotStateCode)
            
            self.stateCodeTxt.setTitle(currentCell, for: .normal)
            
            self.stateCityTableView.isHidden = true
            
        } else if self.BtnTag == 1 {
            
            let currentCell = self.stateName[indexPath.row]
            xprint(currentCell)
            xprint(self.stateNameDic[currentCell])
            
            self.gotCityCode = self.cityNameDic[currentCell]!
            
            self.cityCodeTxt.setTitle(currentCell, for: .normal)
            
            self.stateCityTableView.isHidden = true
        }
    }

//    override func viewDidAppear(_ animated: Bool) {
//        searchTxtFieldItem()
//    }
    
    func initVC() {
        self.emailTxt.attributedPlaceholder = NSAttributedString(string: "ایمیل",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
//        self.stateCodeTxt.attributedPlaceholder = NSAttributedString(string: "استان",
//                                                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
//        self.cityCodeTxt.attributedPlaceholder = NSAttributedString(string: "شهر",
//                                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        
        self.addressTxt.text = self.responseIndividualAndImage.getAdress()
        
        self.addressTxt.attributedPlaceholder = NSAttributedString(string: "آدرس",
                                                                   attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        
        self.postalCodeTxt.attributedPlaceholder = NSAttributedString(string: "کد پستی",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        self.postalCodeTxt.text = self.responseIndividualAndImage.getPostalCode()
        
        self.jobTxt.attributedPlaceholder = NSAttributedString(string: "شغل",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.homeNumTxt.attributedPlaceholder = NSAttributedString(string: "شماره منزل",
                                                                   attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        // Add Image And Font And Title For NavigationBar
//        let attributes = [NSAttributedString.Key.font: UIFont(name: "IRANSans-Bold", size: 20)!]
        self.navigationController?.navigationBar.backgroundColor = UIColor(hexString: "#1456A8")
//        UINavigationBar.appearance().titleTextAttributes = attributes
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: nil)
        navigationItem.backBarButtonItem = backButton
        self.navigationItem.title = ""
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        hideKeyboardWhenTappedAround()
        
//        self.stateCodeTxt.delegate = self
//        self.cityCodeTxt.delegate = self
    }
    
    func buildCSR() {
        let sizeOfKey = keyAlgorithm.availableKeySizes.last!
        
        let (potentialPrivateKey,potentialPublicKey) = self.generateKeysAndStoreInKeychain(keyAlgorithm, keySize: sizeOfKey, tagPrivate: tagPrivate, tagPublic: tagPublic)
        let privateKey = potentialPrivateKey
        let publicKey = potentialPublicKey
        
        let (potentialPublicKeyBits, potentialPublicKeyBlockSize) = self.getPublicKeyBits(keyAlgorithm, publicKey: publicKey!, tagPublic: tagPublic)
        let publicKeyBits = potentialPublicKeyBits
        let _ = potentialPublicKeyBlockSize
        
        let csr = CertificateSigningRequest(commonName: self.englishFirstName.text! + self.englishLastName.text!, organizationName: "Test", organizationUnitName: "Test", countryName: "IR", stateOrProvinceName: "THR", localityName: "Test", keyAlgorithm: keyAlgorithm)
        
        xprint(publicKeyBits!)
        xprint(privateKey!)
        
        //Build the CSR
        let csrBuild = csr.buildAndEncodeDataAsString(publicKeyBits!, privateKey: privateKey!)
        
        if let csrRegular = csrBuild{
            xprint("CSR string no header and footer")
            xprint(csrRegular)
            postSignCSR(email: self.emailTxt.text!, CSR: csrRegular)
        } else {
            // err
        }
        //        let csrBuild2 = csr.buildCSRAndReturnString(publicKeyBits!, privateKey: privateKey!)
        //        if let csrWithHeaderFooter = csrBuild2{
        //            xprint("CSR string with header and footer")
        //            xprint(csrWithHeaderFooter)
        //
        //        }
    }
    
    func getPublicKeyBits(_ algorithm: KeyAlgorithm, publicKey: SecKey, tagPublic: String)->(Data?,Int?) {
        //Set block size
        let keyBlockSize = SecKeyGetBlockSize(publicKey)
        //Ask keychain to provide the publicKey in bits
        let query: [String: AnyObject] = [
            String(kSecClass): kSecClassKey,
            String(kSecAttrKeyType): algorithm.secKeyAttrType,
            String(kSecAttrApplicationTag): tagPublic as AnyObject,
            String(kSecReturnData): kCFBooleanTrue
        ]
        var tempPublicKeyBits:AnyObject?
        var _ = SecItemCopyMatching(query as CFDictionary, &tempPublicKeyBits)
        guard let keyBits = tempPublicKeyBits as? Data else {
            return (nil,nil)
        }
        return (keyBits,keyBlockSize)
    }
    
    //
    func generateKeysAndStoreInKeychain(_ algorithm: KeyAlgorithm, keySize: Int, tagPrivate: String, tagPublic: String)->(SecKey?,SecKey?){
        let publicKeyParameters: [String: AnyObject] = [
            String(kSecAttrIsPermanent): kCFBooleanTrue,
            String(kSecAttrApplicationTag): tagPublic as AnyObject,
            String(kSecAttrAccessible): kSecAttrAccessibleAfterFirstUnlock
        ]
        
        var privateKeyParameters: [String: AnyObject] = [
            String(kSecAttrIsPermanent): kCFBooleanTrue,
            String(kSecAttrApplicationTag): tagPrivate as AnyObject,
            String(kSecAttrAccessible): kSecAttrAccessibleAfterFirstUnlock
        ]
        
        #if !targetEnvironment(simulator)
            //This only works for Secure Enclave consistign of 256 bit key, note, the signatureType is irrelavent for this check
            if algorithm.type == KeyAlgorithm.ec(signatureType: .sha1).type{
                let access = SecAccessControlCreateWithFlags(kCFAllocatorDefault,
                                                             kSecAttrAccessibleAfterFirstUnlock,
                                                             .privateKeyUsage,
                                                             nil)!   // Ignore error
                
                privateKeyParameters[String(kSecAttrAccessControl)] = access
            }
        #endif
        
        //Define what type of keys to be generated here
        var parameters: [String: AnyObject] = [
            String(kSecAttrKeyType): algorithm.secKeyAttrType,
            String(kSecAttrKeySizeInBits): keySize as AnyObject,
            String(kSecReturnRef): kCFBooleanTrue,
            String(kSecPublicKeyAttrs): publicKeyParameters as AnyObject,
            String(kSecPrivateKeyAttrs): privateKeyParameters as AnyObject,
        ]
        
        #if !targetEnvironment(simulator)
            //iOS only allows EC 256 keys to be secured in enclave. This will attempt to allow any EC key in the enclave, assuming iOS will do it outside of the enclave if it doesn't like the key size, note: the signatureType is irrelavent for this check
            if algorithm.type == KeyAlgorithm.ec(signatureType: .sha1).type{
                parameters[String(kSecAttrTokenID)] = kSecAttrTokenIDSecureEnclave
            }
        #endif
        
        //Use Apple Security Framework to generate keys, save them to application keychain
        var error: Unmanaged<CFError>?
        let privateKey = SecKeyCreateRandomKey(parameters as CFDictionary, &error)
        if privateKey == nil{
            print("Error creating keys occured: \(error!.takeRetainedValue() as Error), keys weren't created")
            return (nil,nil)
        }
        
        //Get generated public key
        let query: [String: AnyObject] = [
            String(kSecClass): kSecClassKey,
            String(kSecAttrKeyType): algorithm.secKeyAttrType,
            String(kSecAttrApplicationTag): tagPublic as AnyObject,
            String(kSecReturnRef): kCFBooleanTrue
        ]
        var publicKeyReturn:AnyObject?
        let result = SecItemCopyMatching(query as CFDictionary, &publicKeyReturn)
        if result != errSecSuccess{
            print("Error getting publicKey fron keychain occured: \(result)")
            return (privateKey,nil)
        }
        let publicKey = publicKeyReturn as! SecKey?
        return (privateKey,publicKey)
    }
    
    //MARK - Email Validation
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func startAnimating(){
        let size = CGSize(width: 70, height: 70)
        let font = UIFont(name: "IRANSans-Light", size: 15)
        startAnimating(size, message: "در حال بارگذاری...", messageFont: font, type: NVActivityIndicatorType.ballClipRotateMultiple, color: #colorLiteral(red: 0.08122608811, green: 0.4230399728, blue: 0.7173703313, alpha: 1),
                       displayTimeThreshold: 4, minimumDisplayTime: 2, textColor: UIColor.white)
    }
    
//    func searchTxtFieldItem() {
//        self.stateCodeTxt.itemSelectionHandler = {item, itemPosition in
//            xprint(item, itemPosition)
//            self.stateCodeTxt.text = item[itemPosition].title
//            self.gotStateCode = self.stateNameDic[item[itemPosition].title]!
//            self.getCityCode(URLState: self.gotStateCode)
//            self.cityCodeTxt.resignFirstResponder()
//        }
//
//        self.cityCodeTxt.itemSelectionHandler = {item, itemPosition in
//            self.cityCodeTxt.text = item[itemPosition].title
//            self.gotCityCode = self.cityNameDic[item[itemPosition].title]!
//        }
//    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                if self.englishFirstName.isFirstResponder || self.englishLastName.isFirstResponder || self.emailTxt.isFirstResponder || self.stateCodeTxt.isFirstResponder || self.cityCodeTxt.isFirstResponder {
                    self.view.frame.origin.y = 0
                } else {
                    self.view.frame.origin.y -= keyboardSize.height - 30
                }
                
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    //CallWebservice
    func postSignCSR(email: String , CSR: String) {
        self.startAnimating()
        Webservic.sheared.postSignCSR(englishFirstName: self.englishFirstName.text!, englishLastName: self.englishLastName.text!, address: /*self.responseIndividualAndImage.getAdress()*/self.addressTxt.text!, cityCode: self.gotCityCode, email: email, jobTitle: self.jobTxt.text!, homeNumber: self.homeNumTxt.text!, postalCode: self.postalCodeTxt.text!, stateCode: self.gotStateCode, csr: CSR) { (response, err) in
            if response == nil {
                self.stopAnimating()
                self.view.makeToast("مشکلی در اتصال به شبکه پیش آمده لطفا بعدا امتحان کنید.", duration: 2, position: .center)
            } else {
                self.stopAnimating()
                xprint(response)
                
                self.responsePostSignCSR = response!
                
                var string_Data = KeyChain.stringToNSDATA(string: self.responsePostSignCSR.getTraceID())
                KeyChain.save(key: self.tagtraceID, data: string_Data)
                
                self.signatureCSR()
                
                xprint(self.phoneNumber.english())
                UserDefaults.standard.set(true, forKey: self.phoneNumber.english())
                
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
    
    func getStateCode() {
        self.startAnimating()
        Webservic.sheared.getState { (response, error) in
            self.stateCode = response!
            self.cityCodeTxt.isEnabled = false

            for item in 0..<self.stateCode.count {

                self.stateNameDic.updateValue(self.stateCode[item].getStateCode(), forKey: self.stateCode[item].getStateName())

                self.stateName.append(self.stateCode[item].getStateName())

            }
            
            
            xprint("getStateCode Done")
            self.stateCityTableView.reloadData()
//            self.stateCodeTxt.filterStrings(self.stateName)
            
//            self.stateCodeTxt.itemSelectionHandler = {item, itemPosition in
//                xprint(item, itemPosition)
//            }
            
            self.stopAnimating()
            
            
        }
    }
    
    func getCityCode(URLState: Int) {
        self.startAnimating()
        Webservic.sheared.getCity(urlFromState: URLState) { (response, error) in
            self.cityCode = response!
            self.cityCodeTxt.isEnabled = true

            for item in 0..<self.cityCode.count {

                self.cityNameDic.updateValue(self.cityCode[item].getCityCode(), forKey: self.cityCode[item].getCityName())

                self.cityName.append(self.cityCode[item].getCityName())
            }
            
            xprint("getCityCode Done")
            self.stateCityTableView.reloadData()
            
//            self.cityCodeTxt.filterStrings(self.cityName)
            
            self.stopAnimating()
        }
    }
    
    func signatureCSR() {
        let tagPrivate = "com.csr.private.rsa"
        
        let udid = UIDevice.current.identifierForVendor!.uuidString //UUID().uuidString
        xprint(udid)
        
        let documnets = self.phoneNumber + ":" + udid
        xprint(documnets)
        
        let sign = Helper.shared.getPrivateKeyFromKeychainAndSign(tagPrivate: tagPrivate, data: documnets.data(using: .utf8)! as CFData)
        xprint(sign)
    }
    
    @IBAction func stateBtnPressed(_ sender: Any) {
        self.BtnTag = 0
        self.stateCityTableView.isHidden = false
    }
    
    @IBAction func cityBtnPressed(_ sender: Any) {
        self.BtnTag = 1
        self.stateCityTableView.isHidden = false
    }
    
    @IBAction func checkBoxBtnPressed(_ sender: UIButton) {
        if sender.isSelected == false {
            self.checkBoxBtn.isSelected = true
            self.checkBoxBtn.setImage(UIImage(named: "check box"), for: .normal)
            self.confirmBtn.isEnabled = true
            self.confirmBtn.backgroundColor = #colorLiteral(red: 0.07843137255, green: 0.337254902, blue: 0.6588235294, alpha: 1)
        } else {
            self.checkBoxBtn.isSelected = false
            self.checkBoxBtn.setImage(UIImage(named: "deselctBox"), for: .normal)
            self.confirmBtn.isEnabled = false
            self.confirmBtn.backgroundColor = #colorLiteral(red: 0.7607843137, green: 0.7607843137, blue: 0.7607843137, alpha: 1)
        }
    }
    
    @IBAction func confirmBtnPressed(_ sender: Any) {
        if isValidEmail(testStr: self.emailTxt.text!) {
            if self.stateCodeTxt.currentTitle != "شهر محل زندگی خود را  انتخاب کنید" && self.stateCodeTxt.currentTitle != "استان محل زندگی خود را انتخاب کنید" {
                if self.addressTxt.text != nil {
                    if self.postalCodeTxt.text != nil {
                        if self.englishFirstName.text != "" {
                            if self.englishLastName.text != "" {
                                
                                buildCSR()
                                
                                //postSignCSR(email: self.emailTxt.text!, CSR: self.careatedPublicKey)
                            } else {
                                self.view.makeToast("نام خانوادگی خود را به انگلیسی وارد کنید.", duration: 2, position: .center)
                            }
                        } else {
                            self.view.makeToast("اسم خود را به انگلیسی وارد کنید.", duration: 2, position: .center)
                        }
                    } else {
                        self.view.makeToast("کد پستی را بررسی کنید.", duration: 2, position: .center)
                    }
                } else {
                    self.view.makeToast("آدرس را بررسی کنید.", duration: 2, position: .center)
                }
            } else {
                self.view.makeToast("استان یا شهر خود را بررسی کنید.", duration: 2, position: .center)
            }
        } else {
            self.view.makeToast("ایمیل معتبر نیست.", duration: 2, position: .center)
        }
    }
    
}
