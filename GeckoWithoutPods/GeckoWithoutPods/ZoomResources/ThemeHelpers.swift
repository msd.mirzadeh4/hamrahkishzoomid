//
//  ThemeHelpers.swift
//  ZoomAuthentication-Sample-App
//
//  Created by Tyler Kistler on 12/16/19.
//  Copyright © 2019 FaceTec, Inc. All rights reserved.
//

import Foundation
import UIKit
import ZoomAuthentication

class ThemeHelpers {

    public class func setAppTheme(theme: String) {
        ZoomGlobalState.currentCustomization = getCustomizationForTheme(theme: theme)
        let currentLowLightCustomization: ZoomCustomization? = getLowLightCustomizationForTheme(theme: theme)
        
        Zoom.sdk.setCustomization(ZoomGlobalState.currentCustomization)
        Zoom.sdk.setLowLightCustomization(currentLowLightCustomization)
    }
    
    class func getCustomizationForTheme(theme: String) -> ZoomCustomization {
        let currentCustomization = ZoomCustomization()
        
        let retryScreenSlideshowImages = [UIImage(named: "zoom_ideal_1")!, UIImage(named: "zoom_ideal_2")!, UIImage(named: "zoom_ideal_3")!, UIImage(named: "zoom_ideal_4")!, UIImage(named: "zoom_ideal_5")!]
        
        if theme == "FaceTec Theme" {
            // using default customizations -- do nothing
//            currentCustomization.overlayCustomization.brandingImage = UIimage
            currentCustomization.overlayCustomization.showBrandingImage = false
        }
        else if theme == "Pseudo-Fullscreen" {
            let primaryColor = UIColor(red: 0.169, green: 0.169, blue: 0.169, alpha: 1) // black
            let secondaryColor = UIColor(red: 0.235, green: 0.702, blue: 0.443, alpha: 1) // green
            let backgroundColor = UIColor(red: 0.933, green: 0.965, blue: 0.973, alpha: 1) // white
            let backgroundLayer = CAGradientLayer.init()
            backgroundLayer.colors = [secondaryColor.cgColor, secondaryColor.cgColor]
            backgroundLayer.locations = [0,1]
            backgroundLayer.startPoint = CGPoint.init(x: 0, y: 0)
            backgroundLayer.endPoint = CGPoint.init(x: 1, y: 0)
            
            var font = UIFont.init(name: "Futura-Medium", size: 26)
            if(font == nil) {
                font = UIFont.systemFont(ofSize: 26)
            }
            
            let zoomFeedbackShadow: ZoomShadow? = nil
            let zoomFrameShadow: ZoomShadow? = nil
            
            //
            // NOTE: For this theme, the Result Screen's activity indicator and result animations are overriden by the use of the ZoomCustomAnimationDelegate and its methods to specify a custom UIView to display for the individual animations.
            //
            
            // Overlay Customization
            currentCustomization.overlayCustomization.blurEffectStyle = ZoomBlurEffectStyle.off
            currentCustomization.overlayCustomization.blurEffectOpacity = 0
            currentCustomization.overlayCustomization.backgroundColor = backgroundColor
            currentCustomization.overlayCustomization.showBrandingImage = false
            currentCustomization.overlayCustomization.brandingImage = nil
            // Guidance Customization
            currentCustomization.guidanceCustomization.backgroundColors = [backgroundColor, backgroundColor]
            currentCustomization.guidanceCustomization.foregroundColor = primaryColor
            currentCustomization.guidanceCustomization.headerFont = font!.withSize(26)
            currentCustomization.guidanceCustomization.headerTextSpacing = 0
            currentCustomization.guidanceCustomization.subtextFont = font!.withSize(16)
            currentCustomization.guidanceCustomization.subtextTextSpacing = 0
            currentCustomization.guidanceCustomization.buttonFont = font!.withSize(20)
            currentCustomization.guidanceCustomization.buttonTextSpacing = 0
            currentCustomization.guidanceCustomization.buttonTextNormalColor = backgroundColor
            currentCustomization.guidanceCustomization.buttonBackgroundNormalColor = primaryColor
            currentCustomization.guidanceCustomization.buttonTextHighlightColor = backgroundColor
            currentCustomization.guidanceCustomization.buttonBackgroundHighlightColor = UIColor(red: 0.337, green: 0.337, blue: 0.337, alpha: 1)
            currentCustomization.guidanceCustomization.buttonTextDisabledColor = backgroundColor
            currentCustomization.guidanceCustomization.buttonBackgroundDisabledColor =  primaryColor
            currentCustomization.guidanceCustomization.buttonBorderColor = UIColor.clear
            currentCustomization.guidanceCustomization.buttonBorderWidth = 0
            currentCustomization.guidanceCustomization.buttonCornerRadius = 30
            currentCustomization.guidanceCustomization.buttonRelativeWidth = 1.0
            currentCustomization.guidanceCustomization.readyScreenOvalFillColor = UIColor.clear
            currentCustomization.guidanceCustomization.readyScreenTextBackgroundColor = backgroundColor
            currentCustomization.guidanceCustomization.readyScreenTextBackgroundCornerRadius = 5
            currentCustomization.guidanceCustomization.retryScreenImageBorderColor = primaryColor
            currentCustomization.guidanceCustomization.retryScreenImageBorderWidth = 2
            currentCustomization.guidanceCustomization.retryScreenImageCornerRadius = 10
            currentCustomization.guidanceCustomization.retryScreenOvalStrokeColor = backgroundColor
            currentCustomization.guidanceCustomization.retryScreenSlideshowImages = retryScreenSlideshowImages
            currentCustomization.guidanceCustomization.retryScreenSlideshowInterval = 2000
            currentCustomization.guidanceCustomization.enableRetryScreenSlideshowShuffle = true
            currentCustomization.guidanceCustomization.enableRetryScreenBulletedInstructions = true
            currentCustomization.guidanceCustomization.cameraPermissionsScreenImage = UIImage(named: "camera_shutter_black")
            // ID Scan Customization
            currentCustomization.idScanCustomization.showSelectionScreenBrandingImage = false
            currentCustomization.idScanCustomization.selectionScreenBrandingImage = nil
            currentCustomization.idScanCustomization.selectionScreenBackgroundColors = [backgroundColor, backgroundColor]
            currentCustomization.idScanCustomization.reviewScreenBackgroundColors = [backgroundColor, backgroundColor]
            currentCustomization.idScanCustomization.captureScreenForegroundColor = primaryColor
            currentCustomization.idScanCustomization.reviewScreenForegroundColor = primaryColor
            currentCustomization.idScanCustomization.selectionScreenForegroundColor = primaryColor
            currentCustomization.idScanCustomization.captureScreenFocusMessageTextColor = UIColor(red: 0.337, green: 0.337, blue: 0.337, alpha: 1)
            currentCustomization.idScanCustomization.headerFont = font!.withSize(26)
            currentCustomization.idScanCustomization.headerTextSpacing = 0
            currentCustomization.idScanCustomization.subtextFont = font!.withSize(16)
            currentCustomization.idScanCustomization.subtextTextSpacing = 0
            currentCustomization.idScanCustomization.buttonFont = font!.withSize(20)
            currentCustomization.idScanCustomization.buttonTextSpacing = 0
            currentCustomization.idScanCustomization.buttonTextNormalColor = backgroundColor
            currentCustomization.idScanCustomization.buttonBackgroundNormalColor = primaryColor
            currentCustomization.idScanCustomization.buttonTextHighlightColor = backgroundColor
            currentCustomization.idScanCustomization.buttonBackgroundHighlightColor = UIColor(red: 0.337, green: 0.337, blue: 0.337, alpha: 1)
            currentCustomization.idScanCustomization.buttonTextDisabledColor = backgroundColor
            currentCustomization.idScanCustomization.buttonBackgroundDisabledColor =  primaryColor
            currentCustomization.idScanCustomization.buttonBorderColor = UIColor.clear
            currentCustomization.idScanCustomization.buttonBorderWidth = 0
            currentCustomization.idScanCustomization.buttonCornerRadius = 30
            currentCustomization.idScanCustomization.buttonRelativeWidth = 1.0
            currentCustomization.idScanCustomization.captureScreenTextBackgroundColor = backgroundColor
            currentCustomization.idScanCustomization.captureScreenTextBackgroundBorderColor = primaryColor
            currentCustomization.idScanCustomization.captureScreenTextBackgroundBorderWidth = 2
            currentCustomization.idScanCustomization.captureScreenTextBackgroundCornerRadius = 5
            currentCustomization.idScanCustomization.reviewScreenTextBackgroundColor = backgroundColor
            currentCustomization.idScanCustomization.reviewScreenTextBackgroundBorderColor = primaryColor
            currentCustomization.idScanCustomization.reviewScreenTextBackgroundBorderWidth = 2
            currentCustomization.idScanCustomization.reviewScreenTextBackgroundCornerRadius = 5
            currentCustomization.idScanCustomization.captureScreenBackgroundColor = backgroundColor
            currentCustomization.idScanCustomization.captureFrameStrokeColor = primaryColor
            currentCustomization.idScanCustomization.captureFrameStrokeWith = 2
            currentCustomization.idScanCustomization.captureFrameCornerRadius = 12
            currentCustomization.idScanCustomization.activeTorchButtonImage = UIImage(named: "zoom_active_torch")
            currentCustomization.idScanCustomization.inactiveTorchButtonImage = UIImage(named: "zoom_inactive_torch")
            // Result Screen Customization
            currentCustomization.resultScreenCustomization.backgroundColors = [backgroundColor, backgroundColor]
            currentCustomization.resultScreenCustomization.foregroundColor = primaryColor
            currentCustomization.resultScreenCustomization.messageFont = font!.withSize(20)
            currentCustomization.resultScreenCustomization.messageTextSpacing = 0
            currentCustomization.resultScreenCustomization.activityIndicatorColor = primaryColor
            currentCustomization.resultScreenCustomization.customActivityIndicatorImage = nil
            currentCustomization.resultScreenCustomization.customActivityIndicatorRotationInterval = 800
            currentCustomization.resultScreenCustomization.resultAnimationBackgroundColor = secondaryColor
            currentCustomization.resultScreenCustomization.resultAnimationForegroundColor = backgroundColor
            currentCustomization.resultScreenCustomization.resultAnimationSuccessBackgroundImage = nil
            currentCustomization.resultScreenCustomization.resultAnimationUnsuccessBackgroundImage = nil
            currentCustomization.resultScreenCustomization.showUploadProgressBar = true
            currentCustomization.resultScreenCustomization.uploadProgressTrackColor = primaryColor.withAlphaComponent(0.2)
            currentCustomization.resultScreenCustomization.uploadProgressFillColor = secondaryColor
            currentCustomization.resultScreenCustomization.animationRelativeScale = 1.0
            // Feedback Customization
            currentCustomization.feedbackCustomization.backgroundColor = backgroundLayer
            currentCustomization.feedbackCustomization.textColor = backgroundColor
            currentCustomization.feedbackCustomization.textFont = font!.withSize(20)
            currentCustomization.feedbackCustomization.textSpacing = 0
            currentCustomization.feedbackCustomization.cornerRadius = 5
            currentCustomization.feedbackCustomization.shadow = zoomFeedbackShadow
            currentCustomization.feedbackCustomization.relativeWidth = 1.0
            // Frame Customization
            currentCustomization.frameCustomization.backgroundColor = backgroundColor
            currentCustomization.frameCustomization.borderColor = primaryColor
            currentCustomization.frameCustomization.borderWidth = 0
            currentCustomization.frameCustomization.cornerRadius = 0
            currentCustomization.frameCustomization.shadow = zoomFrameShadow
            // Oval Customization
            currentCustomization.ovalCustomization.strokeColor = primaryColor
            currentCustomization.ovalCustomization.progressColor1 = secondaryColor.withAlphaComponent(0.7)
            currentCustomization.ovalCustomization.progressColor2 = secondaryColor.withAlphaComponent(0.7)
            // Cancel Button Customization
            currentCustomization.cancelButtonCustomization.customImage = UIImage(named: "single_chevron_left_black")
            currentCustomization.cancelButtonCustomization.location = ZoomCancelButtonLocation.custom
            currentCustomization.cancelButtonCustomization.customLocation = CGRect(x: 20, y: 30, width: 25, height: 25)
            
            // Guidance Customization -- Text Style Overrides
            // Ready Screen Header
            currentCustomization.guidanceCustomization.readyScreenHeaderFont = font!.withSize(26)
            currentCustomization.guidanceCustomization.readyScreenHeaderTextSpacing = 0
            currentCustomization.guidanceCustomization.readyScreenHeaderTextColor = primaryColor
            // Ready Screen Subtext
            currentCustomization.guidanceCustomization.readyScreenSubtextFont = font!.withSize(14)
            currentCustomization.guidanceCustomization.readyScreenSubtextTextSpacing = 0
            currentCustomization.guidanceCustomization.readyScreenSubtextTextColor = UIColor(red: 0.337, green: 0.337, blue: 0.337, alpha: 1)
            // Retry Screen Header
            currentCustomization.guidanceCustomization.retryScreenHeaderFont = font!.withSize(26)
            currentCustomization.guidanceCustomization.retryScreenHeaderTextSpacing = 0
            currentCustomization.guidanceCustomization.retryScreenHeaderTextColor = primaryColor
            // Retry Screen Subtext
            currentCustomization.guidanceCustomization.retryScreenSubtextFont = font!.withSize(16)
            currentCustomization.guidanceCustomization.retryScreenSubtextTextSpacing = 0
            currentCustomization.guidanceCustomization.retryScreenSubtextTextColor = UIColor(red: 0.337, green: 0.337, blue: 0.337, alpha: 1)
        }
        else if theme == "Well-Rounded" {
            let primaryColor = UIColor(red: 0.035, green: 0.710, blue: 0.639, alpha: 1) // green
            let backgroundColor = UIColor.white
            let backgroundLayer = CAGradientLayer.init()
            backgroundLayer.colors = [primaryColor.cgColor, primaryColor.cgColor]
            backgroundLayer.locations = [0,1]
            backgroundLayer.startPoint = CGPoint.init(x: 0, y: 0)
            backgroundLayer.endPoint = CGPoint.init(x: 1, y: 0)
            
            let zoomFeedbackShadow: ZoomShadow? = ZoomShadow(color: UIColor.black, opacity: 0.5, radius: 2, offset: CGSize(width: 0, height: 0), insets: UIEdgeInsets(top: 1, left: -1, bottom: -1, right: -1))
            let zoomFrameShadow: ZoomShadow? = ZoomShadow(color: UIColor.black, opacity: 0.5, radius: 4, offset: CGSize(width: 0, height: 0), insets: UIEdgeInsets(top: 1, left: -1, bottom: -1, right: -1))
            
            //
            // NOTE: For this theme, the Result Screen's activity indicator and result animations are overriden by the use of the ZoomCustomAnimationDelegate and its methods to specify a custom UIView to display for the individual animations.
            //
            
            // Overlay Customization
            currentCustomization.overlayCustomization.blurEffectStyle = ZoomBlurEffectStyle.off
            currentCustomization.overlayCustomization.blurEffectOpacity = 0
            currentCustomization.overlayCustomization.backgroundColor = UIColor.clear
            currentCustomization.overlayCustomization.showBrandingImage = false
            currentCustomization.overlayCustomization.brandingImage = nil
            // Guidance Customization
            currentCustomization.guidanceCustomization.backgroundColors = [backgroundColor, backgroundColor]
            currentCustomization.guidanceCustomization.foregroundColor = primaryColor
            currentCustomization.guidanceCustomization.headerFont = UIFont.systemFont(ofSize: 26, weight: UIFont.Weight.init(0.3))
            currentCustomization.guidanceCustomization.headerTextSpacing = 1.5
            currentCustomization.guidanceCustomization.subtextFont = UIFont.systemFont(ofSize: 16, weight: .light)
            currentCustomization.guidanceCustomization.subtextTextSpacing = 0
            currentCustomization.guidanceCustomization.buttonFont = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.init(0.4))
            currentCustomization.guidanceCustomization.buttonTextSpacing = 1.5
            currentCustomization.guidanceCustomization.buttonTextNormalColor = backgroundColor
            currentCustomization.guidanceCustomization.buttonBackgroundNormalColor = primaryColor
            currentCustomization.guidanceCustomization.buttonTextHighlightColor = backgroundColor
            currentCustomization.guidanceCustomization.buttonBackgroundHighlightColor = UIColor(red: 0.192, green: 0.867, blue: 0.796, alpha: 1)
            currentCustomization.guidanceCustomization.buttonTextDisabledColor = UIColor(red: 0.843, green: 0.843, blue: 0.843, alpha: 1)
            currentCustomization.guidanceCustomization.buttonBackgroundDisabledColor =  UIColor(red: 0, green: 0.553, blue: 0.482, alpha: 1)
            currentCustomization.guidanceCustomization.buttonBorderColor = UIColor.clear
            currentCustomization.guidanceCustomization.buttonBorderWidth = 0
            currentCustomization.guidanceCustomization.buttonCornerRadius = 30
            currentCustomization.guidanceCustomization.buttonRelativeWidth = 0.75
            currentCustomization.guidanceCustomization.readyScreenOvalFillColor = primaryColor.withAlphaComponent(0.2)
            currentCustomization.guidanceCustomization.readyScreenTextBackgroundColor = backgroundColor
            currentCustomization.guidanceCustomization.readyScreenTextBackgroundCornerRadius = 5
            currentCustomization.guidanceCustomization.retryScreenImageBorderColor = primaryColor
            currentCustomization.guidanceCustomization.retryScreenImageBorderWidth = 2
            currentCustomization.guidanceCustomization.retryScreenImageCornerRadius = 10
            currentCustomization.guidanceCustomization.retryScreenOvalStrokeColor = backgroundColor
            currentCustomization.guidanceCustomization.retryScreenSlideshowImages = []
            currentCustomization.guidanceCustomization.retryScreenSlideshowInterval = 1500
            currentCustomization.guidanceCustomization.enableRetryScreenSlideshowShuffle = true
            currentCustomization.guidanceCustomization.enableRetryScreenBulletedInstructions = true
            currentCustomization.guidanceCustomization.cameraPermissionsScreenImage = UIImage(named: "camera_green")
            // ID Scan Customization
            currentCustomization.idScanCustomization.showSelectionScreenBrandingImage = false
            currentCustomization.idScanCustomization.selectionScreenBrandingImage = nil
            currentCustomization.idScanCustomization.selectionScreenBackgroundColors = [backgroundColor, backgroundColor]
            currentCustomization.idScanCustomization.reviewScreenBackgroundColors = [backgroundColor, backgroundColor]
            currentCustomization.idScanCustomization.captureScreenForegroundColor = primaryColor
            currentCustomization.idScanCustomization.reviewScreenForegroundColor = primaryColor
            currentCustomization.idScanCustomization.selectionScreenForegroundColor = primaryColor
            currentCustomization.idScanCustomization.captureScreenFocusMessageTextColor = primaryColor
            currentCustomization.idScanCustomization.headerFont = UIFont.systemFont(ofSize: 26, weight: UIFont.Weight.init(0.3))
            currentCustomization.idScanCustomization.headerTextSpacing = 1.5
            currentCustomization.idScanCustomization.subtextFont = UIFont.systemFont(ofSize: 16, weight: .light)
            currentCustomization.idScanCustomization.subtextTextSpacing = 0
            currentCustomization.idScanCustomization.buttonFont = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.init(0.4))
            currentCustomization.idScanCustomization.buttonTextSpacing = 1.5
            currentCustomization.idScanCustomization.buttonTextNormalColor = backgroundColor
            currentCustomization.idScanCustomization.buttonBackgroundNormalColor = primaryColor
            currentCustomization.idScanCustomization.buttonTextHighlightColor = backgroundColor
            currentCustomization.idScanCustomization.buttonBackgroundHighlightColor = UIColor(red: 0.192, green: 0.867, blue: 0.796, alpha: 1)
            currentCustomization.idScanCustomization.buttonTextDisabledColor = UIColor(red: 0.843, green: 0.843, blue: 0.843, alpha: 1)
            currentCustomization.idScanCustomization.buttonBackgroundDisabledColor =  UIColor(red: 0, green: 0.553, blue: 0.482, alpha: 1)
            currentCustomization.idScanCustomization.buttonBorderColor = UIColor.clear
            currentCustomization.idScanCustomization.buttonBorderWidth = 0
            currentCustomization.idScanCustomization.buttonCornerRadius = 30
            currentCustomization.idScanCustomization.buttonRelativeWidth = 0.75
            currentCustomization.idScanCustomization.captureScreenTextBackgroundColor = backgroundColor
            currentCustomization.idScanCustomization.captureScreenTextBackgroundBorderColor = primaryColor
            currentCustomization.idScanCustomization.captureScreenTextBackgroundBorderWidth = 2
            currentCustomization.idScanCustomization.captureScreenTextBackgroundCornerRadius = 5
            currentCustomization.idScanCustomization.reviewScreenTextBackgroundColor = backgroundColor
            currentCustomization.idScanCustomization.reviewScreenTextBackgroundBorderColor = primaryColor
            currentCustomization.idScanCustomization.reviewScreenTextBackgroundBorderWidth = 2
            currentCustomization.idScanCustomization.reviewScreenTextBackgroundCornerRadius = 5
            currentCustomization.idScanCustomization.captureScreenBackgroundColor = backgroundColor
            currentCustomization.idScanCustomization.captureFrameStrokeColor = primaryColor
            currentCustomization.idScanCustomization.captureFrameStrokeWith = 2
            currentCustomization.idScanCustomization.captureFrameCornerRadius = 12
            currentCustomization.idScanCustomization.activeTorchButtonImage = UIImage(named: "zoom_active_torch")
            currentCustomization.idScanCustomization.inactiveTorchButtonImage = UIImage(named: "zoom_inactive_torch")
            // Result Screen Customization
            currentCustomization.resultScreenCustomization.backgroundColors = [backgroundColor, backgroundColor]
            currentCustomization.resultScreenCustomization.foregroundColor = primaryColor
            currentCustomization.resultScreenCustomization.messageFont = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.init(0.3))
            currentCustomization.resultScreenCustomization.messageTextSpacing = 1.5
            currentCustomization.resultScreenCustomization.activityIndicatorColor = primaryColor
            currentCustomization.resultScreenCustomization.customActivityIndicatorImage = nil
            currentCustomization.resultScreenCustomization.customActivityIndicatorRotationInterval = 1000
            currentCustomization.resultScreenCustomization.resultAnimationBackgroundColor = UIColor.clear
            currentCustomization.resultScreenCustomization.resultAnimationForegroundColor = backgroundColor
            currentCustomization.resultScreenCustomization.resultAnimationSuccessBackgroundImage = nil
            currentCustomization.resultScreenCustomization.resultAnimationUnsuccessBackgroundImage = nil
            currentCustomization.resultScreenCustomization.showUploadProgressBar = false
            currentCustomization.resultScreenCustomization.uploadProgressTrackColor = UIColor.black.withAlphaComponent(0.2)
            currentCustomization.resultScreenCustomization.uploadProgressFillColor = primaryColor
            currentCustomization.resultScreenCustomization.animationRelativeScale = 2.0
            // Feedback Customization
            currentCustomization.feedbackCustomization.backgroundColor = backgroundLayer
            currentCustomization.feedbackCustomization.textColor = backgroundColor
            currentCustomization.feedbackCustomization.textFont = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.init(0.3))
            currentCustomization.feedbackCustomization.textSpacing = 1.5
            currentCustomization.feedbackCustomization.cornerRadius = 5
            currentCustomization.feedbackCustomization.shadow = zoomFeedbackShadow
            currentCustomization.feedbackCustomization.relativeWidth = 0.75
            // Frame Customization
            currentCustomization.frameCustomization.backgroundColor = backgroundColor
            currentCustomization.frameCustomization.borderColor = primaryColor
            currentCustomization.frameCustomization.borderWidth = 2
            currentCustomization.frameCustomization.cornerRadius = 20
            currentCustomization.frameCustomization.shadow = zoomFrameShadow
            // Oval Customization
            currentCustomization.ovalCustomization.strokeColor = primaryColor
            currentCustomization.ovalCustomization.progressColor1 = primaryColor
            currentCustomization.ovalCustomization.progressColor2 = primaryColor
            // Cancel Button Customization
            currentCustomization.cancelButtonCustomization.customImage = UIImage(named: "cancel_round_green")
            currentCustomization.cancelButtonCustomization.location = ZoomCancelButtonLocation.topLeft
        }
        else if theme == "Bitcoin Exchange" {
            let primaryColor = UIColor(red: 0.969, green: 0.588, blue: 0.204, alpha: 1) // orange
            let secondaryColor = UIColor(red: 1, green: 1, blue: 0.188, alpha: 1) // yellow
            let backgroundColor = UIColor(red: 0.259, green: 0.259, blue: 0.259, alpha: 1) // dark grey
            let backgroundLayer = CAGradientLayer.init()
            backgroundLayer.colors = [primaryColor.cgColor, primaryColor.cgColor]
            backgroundLayer.locations = [0,1]
            backgroundLayer.startPoint = CGPoint.init(x: 0, y: 0)
            backgroundLayer.endPoint = CGPoint.init(x: 1, y: 0)
            
            let zoomFeedbackShadow: ZoomShadow? = ZoomShadow(color: backgroundColor, opacity: 1, radius: 3, offset: CGSize(width: 0, height: 2), insets: UIEdgeInsets(top: 1, left: -1, bottom: -1, right: -1))
            let zoomFrameShadow: ZoomShadow? = ZoomShadow(color: backgroundColor, opacity: 1, radius: 3, offset: CGSize(width: 0, height: 2), insets: UIEdgeInsets(top: 1, left: -1, bottom: -1, right: -1))
            
            // Overlay Customization
            currentCustomization.overlayCustomization.blurEffectStyle = ZoomBlurEffectStyle.off
            currentCustomization.overlayCustomization.blurEffectOpacity = 0
            currentCustomization.overlayCustomization.backgroundColor = UIColor.clear
            currentCustomization.overlayCustomization.showBrandingImage = true
            currentCustomization.overlayCustomization.brandingImage = UIImage(named: "bitcoin_exchange_logo")
            // Guidance Customization
            currentCustomization.guidanceCustomization.backgroundColors = [backgroundColor, backgroundColor]
            currentCustomization.guidanceCustomization.foregroundColor = primaryColor
            currentCustomization.guidanceCustomization.headerFont = UIFont.systemFont(ofSize: 26, weight: UIFont.Weight.init(0.3))
            currentCustomization.guidanceCustomization.headerTextSpacing = 1.5
            currentCustomization.guidanceCustomization.subtextFont = UIFont.systemFont(ofSize: 16, weight: .light)
            currentCustomization.guidanceCustomization.subtextTextSpacing = 1.5
            currentCustomization.guidanceCustomization.buttonFont = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.init(0.4))
            currentCustomization.guidanceCustomization.buttonTextSpacing = 1.5
            currentCustomization.guidanceCustomization.buttonTextNormalColor = backgroundColor
            currentCustomization.guidanceCustomization.buttonBackgroundNormalColor = primaryColor
            currentCustomization.guidanceCustomization.buttonTextHighlightColor = backgroundColor
            currentCustomization.guidanceCustomization.buttonBackgroundHighlightColor = primaryColor
            currentCustomization.guidanceCustomization.buttonTextDisabledColor = backgroundColor
            currentCustomization.guidanceCustomization.buttonBackgroundDisabledColor = primaryColor
            currentCustomization.guidanceCustomization.buttonBorderColor = UIColor.clear
            currentCustomization.guidanceCustomization.buttonBorderWidth = 0
            currentCustomization.guidanceCustomization.buttonCornerRadius = 5
            currentCustomization.guidanceCustomization.buttonRelativeWidth = 1.0
            currentCustomization.guidanceCustomization.readyScreenOvalFillColor = primaryColor.withAlphaComponent(0.2)
            currentCustomization.guidanceCustomization.readyScreenTextBackgroundColor = backgroundColor
            currentCustomization.guidanceCustomization.readyScreenTextBackgroundCornerRadius = 5
            currentCustomization.guidanceCustomization.retryScreenImageBorderColor = primaryColor
            currentCustomization.guidanceCustomization.retryScreenImageBorderWidth = 2
            currentCustomization.guidanceCustomization.retryScreenImageCornerRadius = 5
            currentCustomization.guidanceCustomization.retryScreenOvalStrokeColor = primaryColor
            currentCustomization.guidanceCustomization.retryScreenSlideshowImages = []
            currentCustomization.guidanceCustomization.retryScreenSlideshowInterval = 1500
            currentCustomization.guidanceCustomization.enableRetryScreenSlideshowShuffle = true
            currentCustomization.guidanceCustomization.enableRetryScreenBulletedInstructions = true
            currentCustomization.guidanceCustomization.cameraPermissionsScreenImage = UIImage(named: "camera_orange")
            // ID Scan Customization
            currentCustomization.idScanCustomization.showSelectionScreenBrandingImage = false
            currentCustomization.idScanCustomization.selectionScreenBrandingImage = nil
            currentCustomization.idScanCustomization.selectionScreenBackgroundColors = [backgroundColor, backgroundColor]
            currentCustomization.idScanCustomization.reviewScreenBackgroundColors = [backgroundColor, backgroundColor]
            currentCustomization.idScanCustomization.captureScreenForegroundColor = primaryColor
            currentCustomization.idScanCustomization.reviewScreenForegroundColor = primaryColor
            currentCustomization.idScanCustomization.selectionScreenForegroundColor = primaryColor
            currentCustomization.idScanCustomization.captureScreenFocusMessageTextColor = primaryColor
            currentCustomization.idScanCustomization.headerFont = UIFont.systemFont(ofSize: 26, weight: UIFont.Weight.init(0.3))
            currentCustomization.idScanCustomization.headerTextSpacing = 1.5
            currentCustomization.idScanCustomization.subtextFont = UIFont.systemFont(ofSize: 16, weight: .light)
            currentCustomization.idScanCustomization.subtextTextSpacing = 1.5
            currentCustomization.idScanCustomization.buttonFont = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.init(0.4))
            currentCustomization.idScanCustomization.buttonTextSpacing = 1.5
            currentCustomization.idScanCustomization.buttonTextNormalColor = backgroundColor
            currentCustomization.idScanCustomization.buttonBackgroundNormalColor = primaryColor
            currentCustomization.idScanCustomization.buttonTextHighlightColor = backgroundColor
            currentCustomization.idScanCustomization.buttonBackgroundHighlightColor = primaryColor
            currentCustomization.idScanCustomization.buttonTextDisabledColor = backgroundColor
            currentCustomization.idScanCustomization.buttonBackgroundDisabledColor = primaryColor
            currentCustomization.idScanCustomization.buttonBorderColor = UIColor.clear
            currentCustomization.idScanCustomization.buttonBorderWidth = 0
            currentCustomization.idScanCustomization.buttonCornerRadius = 5
            currentCustomization.idScanCustomization.buttonRelativeWidth = 1.0
            currentCustomization.idScanCustomization.captureScreenTextBackgroundColor = backgroundColor
            currentCustomization.idScanCustomization.captureScreenTextBackgroundBorderColor = primaryColor
            currentCustomization.idScanCustomization.captureScreenTextBackgroundBorderWidth = 0
            currentCustomization.idScanCustomization.captureScreenTextBackgroundCornerRadius = 8
            currentCustomization.idScanCustomization.reviewScreenTextBackgroundColor = backgroundColor
            currentCustomization.idScanCustomization.reviewScreenTextBackgroundBorderColor = primaryColor
            currentCustomization.idScanCustomization.reviewScreenTextBackgroundBorderWidth = 0
            currentCustomization.idScanCustomization.reviewScreenTextBackgroundCornerRadius = 8
            currentCustomization.idScanCustomization.captureScreenBackgroundColor = backgroundColor
            currentCustomization.idScanCustomization.captureFrameStrokeColor = primaryColor
            currentCustomization.idScanCustomization.captureFrameStrokeWith = 2
            currentCustomization.idScanCustomization.captureFrameCornerRadius = 12
            currentCustomization.idScanCustomization.activeTorchButtonImage = UIImage(named: "torch_active_orange")
            currentCustomization.idScanCustomization.inactiveTorchButtonImage = UIImage(named: "torch_inactive_orange")
            // Result Screen Customization
            currentCustomization.resultScreenCustomization.backgroundColors = [backgroundColor, backgroundColor]
            currentCustomization.resultScreenCustomization.foregroundColor = primaryColor
            currentCustomization.resultScreenCustomization.messageFont = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.init(0.3))
            currentCustomization.resultScreenCustomization.messageTextSpacing = 1.5
            currentCustomization.resultScreenCustomization.activityIndicatorColor = primaryColor
            currentCustomization.resultScreenCustomization.customActivityIndicatorImage = UIImage(named: "activity_indicator_orange")
            currentCustomization.resultScreenCustomization.customActivityIndicatorRotationInterval = 1500
            currentCustomization.resultScreenCustomization.resultAnimationBackgroundColor = primaryColor
            currentCustomization.resultScreenCustomization.resultAnimationForegroundColor = backgroundColor
            currentCustomization.resultScreenCustomization.resultAnimationSuccessBackgroundImage = nil
            currentCustomization.resultScreenCustomization.resultAnimationUnsuccessBackgroundImage = nil
            currentCustomization.resultScreenCustomization.showUploadProgressBar = true
            currentCustomization.resultScreenCustomization.uploadProgressTrackColor = UIColor.black.withAlphaComponent(0.2)
            currentCustomization.resultScreenCustomization.uploadProgressFillColor = primaryColor
            currentCustomization.resultScreenCustomization.animationRelativeScale = 1.0
            // Feedback Customization
            currentCustomization.feedbackCustomization.backgroundColor = backgroundLayer
            currentCustomization.feedbackCustomization.textColor = backgroundColor
            currentCustomization.feedbackCustomization.textFont = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.init(0.3))
            currentCustomization.feedbackCustomization.textSpacing = 1.5
            currentCustomization.feedbackCustomization.cornerRadius = 5
            currentCustomization.feedbackCustomization.shadow = zoomFeedbackShadow
            currentCustomization.feedbackCustomization.relativeWidth = 1.0
            // Frame Customization
            currentCustomization.frameCustomization.backgroundColor = backgroundColor
            currentCustomization.frameCustomization.borderColor = backgroundColor
            currentCustomization.frameCustomization.borderWidth = 2
            currentCustomization.frameCustomization.cornerRadius = 5
            currentCustomization.frameCustomization.shadow = zoomFrameShadow
            // Oval Customization
            currentCustomization.ovalCustomization.strokeColor = primaryColor
            currentCustomization.ovalCustomization.progressColor1 = secondaryColor
            currentCustomization.ovalCustomization.progressColor2 = secondaryColor
            // Cancel Button Customization
            currentCustomization.cancelButtonCustomization.customImage = UIImage(named: "single_chevron_left_orange")
            currentCustomization.cancelButtonCustomization.location = ZoomCancelButtonLocation.topLeft
            
            // Guidance Customization -- Text Style Overrides
            // Ready Screen Header
            currentCustomization.guidanceCustomization.readyScreenHeaderFont = UIFont.systemFont(ofSize: 24, weight: UIFont.Weight.init(0.3))
            currentCustomization.guidanceCustomization.readyScreenHeaderTextSpacing = 1.5
            currentCustomization.guidanceCustomization.readyScreenHeaderTextColor = primaryColor
            // Ready Screen Subtext
            currentCustomization.guidanceCustomization.readyScreenSubtextFont = UIFont.systemFont(ofSize: 14, weight: .light)
            currentCustomization.guidanceCustomization.readyScreenSubtextTextSpacing = 1.5
            currentCustomization.guidanceCustomization.readyScreenSubtextTextColor = secondaryColor
            // Retry Screen Header
            currentCustomization.guidanceCustomization.retryScreenHeaderFont = UIFont.systemFont(ofSize: 24, weight: UIFont.Weight.init(0.3))
            currentCustomization.guidanceCustomization.retryScreenHeaderTextSpacing = 1.5
            currentCustomization.guidanceCustomization.retryScreenHeaderTextColor = primaryColor
            // Retry Screen Subtext
            currentCustomization.guidanceCustomization.retryScreenSubtextFont = UIFont.systemFont(ofSize: 14, weight: .light)
            currentCustomization.guidanceCustomization.retryScreenSubtextTextSpacing = 1.5
            currentCustomization.guidanceCustomization.retryScreenSubtextTextColor = secondaryColor
        }
        else if theme == "eKYC" {
            let primaryColor = UIColor(red: 0.929, green: 0.110, blue: 0.141, alpha: 1) // red
            let secondaryColor = UIColor.black
            let backgroundColor = UIColor.white
            let backgroundLayer = CAGradientLayer.init()
            backgroundLayer.colors = [secondaryColor.cgColor, secondaryColor.cgColor]
            backgroundLayer.locations = [0,1]
            backgroundLayer.startPoint = CGPoint.init(x: 0, y: 0)
            backgroundLayer.endPoint = CGPoint.init(x: 1, y: 0)
            
            let zoomFeedbackShadow: ZoomShadow? = ZoomShadow(color: primaryColor, opacity: 1, radius: 5, offset: CGSize(width: 0, height: 2), insets: UIEdgeInsets(top: 1, left: -1, bottom: -1, right: -1))
            let zoomFrameShadow: ZoomShadow? = ZoomShadow(color: primaryColor, opacity: 1, radius: 3, offset: CGSize(width: 0, height: 2), insets: UIEdgeInsets(top: 1, left: -1, bottom: -1, right: -1))
            
            // Overlay Customization
            currentCustomization.overlayCustomization.blurEffectStyle = ZoomBlurEffectStyle.off
            currentCustomization.overlayCustomization.blurEffectOpacity = 0
            currentCustomization.overlayCustomization.backgroundColor = UIColor.clear
            currentCustomization.overlayCustomization.showBrandingImage = true
            currentCustomization.overlayCustomization.brandingImage = UIImage(named: "ekyc_logo")
            // Guidance Customization
            currentCustomization.guidanceCustomization.backgroundColors = [backgroundColor, backgroundColor]
            currentCustomization.guidanceCustomization.foregroundColor = secondaryColor
            currentCustomization.guidanceCustomization.headerFont = UIFont.systemFont(ofSize: 26, weight: UIFont.Weight.init(0.3))
            currentCustomization.guidanceCustomization.headerTextSpacing = 1.5
            currentCustomization.guidanceCustomization.subtextFont = UIFont.systemFont(ofSize: 16, weight: .light)
            currentCustomization.guidanceCustomization.subtextTextSpacing = 0
            currentCustomization.guidanceCustomization.buttonFont = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.init(0.4))
            currentCustomization.guidanceCustomization.buttonTextSpacing = 1.5
            currentCustomization.guidanceCustomization.buttonTextNormalColor = primaryColor
            currentCustomization.guidanceCustomization.buttonBackgroundNormalColor = UIColor.clear
            currentCustomization.guidanceCustomization.buttonTextHighlightColor = backgroundColor
            currentCustomization.guidanceCustomization.buttonBackgroundHighlightColor = primaryColor
            currentCustomization.guidanceCustomization.buttonTextDisabledColor = primaryColor.withAlphaComponent(0.3)
            currentCustomization.guidanceCustomization.buttonBackgroundDisabledColor = UIColor.clear
            currentCustomization.guidanceCustomization.buttonBorderColor = primaryColor
            currentCustomization.guidanceCustomization.buttonBorderWidth = 2
            currentCustomization.guidanceCustomization.buttonCornerRadius = 8
            currentCustomization.guidanceCustomization.buttonRelativeWidth = 1.0
            currentCustomization.guidanceCustomization.readyScreenOvalFillColor = primaryColor.withAlphaComponent(0.2)
            currentCustomization.guidanceCustomization.readyScreenTextBackgroundColor = backgroundColor
            currentCustomization.guidanceCustomization.readyScreenTextBackgroundCornerRadius = 3
            currentCustomization.guidanceCustomization.retryScreenImageBorderColor = primaryColor
            currentCustomization.guidanceCustomization.retryScreenImageBorderWidth = 2
            currentCustomization.guidanceCustomization.retryScreenImageCornerRadius = 3
            currentCustomization.guidanceCustomization.retryScreenOvalStrokeColor = primaryColor
            currentCustomization.guidanceCustomization.retryScreenSlideshowImages = retryScreenSlideshowImages
            currentCustomization.guidanceCustomization.retryScreenSlideshowInterval = 1500
            currentCustomization.guidanceCustomization.enableRetryScreenSlideshowShuffle = true
            currentCustomization.guidanceCustomization.enableRetryScreenBulletedInstructions = true
            currentCustomization.guidanceCustomization.cameraPermissionsScreenImage = UIImage(named: "camera_red")
            // ID Scan Customization
            currentCustomization.idScanCustomization.showSelectionScreenBrandingImage = false
            currentCustomization.idScanCustomization.selectionScreenBrandingImage = nil
            currentCustomization.idScanCustomization.selectionScreenBackgroundColors = [backgroundColor, backgroundColor]
            currentCustomization.idScanCustomization.reviewScreenBackgroundColors = [backgroundColor, backgroundColor]
            currentCustomization.idScanCustomization.captureScreenForegroundColor = backgroundColor
            currentCustomization.idScanCustomization.reviewScreenForegroundColor = backgroundColor
            currentCustomization.idScanCustomization.selectionScreenForegroundColor = primaryColor
            currentCustomization.idScanCustomization.captureScreenFocusMessageTextColor = secondaryColor
            currentCustomization.idScanCustomization.headerFont = UIFont.systemFont(ofSize: 26, weight: UIFont.Weight.init(0.3))
            currentCustomization.idScanCustomization.headerTextSpacing = 1.5
            currentCustomization.idScanCustomization.subtextFont = UIFont.systemFont(ofSize: 16, weight: .light)
            currentCustomization.idScanCustomization.subtextTextSpacing = 0
            currentCustomization.idScanCustomization.buttonFont = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.init(0.4))
            currentCustomization.idScanCustomization.buttonTextSpacing = 1.5
            currentCustomization.idScanCustomization.buttonTextNormalColor = primaryColor
            currentCustomization.idScanCustomization.buttonBackgroundNormalColor = UIColor.clear
            currentCustomization.idScanCustomization.buttonTextHighlightColor = backgroundColor
            currentCustomization.idScanCustomization.buttonBackgroundHighlightColor = primaryColor
            currentCustomization.idScanCustomization.buttonTextDisabledColor = primaryColor.withAlphaComponent(0.3)
            currentCustomization.idScanCustomization.buttonBackgroundDisabledColor = UIColor.clear
            currentCustomization.idScanCustomization.buttonBorderColor = primaryColor
            currentCustomization.idScanCustomization.buttonBorderWidth = 2
            currentCustomization.idScanCustomization.buttonCornerRadius = 8
            currentCustomization.idScanCustomization.buttonRelativeWidth = 1.0
            currentCustomization.idScanCustomization.captureScreenTextBackgroundColor = primaryColor
            currentCustomization.idScanCustomization.captureScreenTextBackgroundBorderColor = primaryColor
            currentCustomization.idScanCustomization.captureScreenTextBackgroundBorderWidth = 0
            currentCustomization.idScanCustomization.captureScreenTextBackgroundCornerRadius = 2
            currentCustomization.idScanCustomization.reviewScreenTextBackgroundColor = primaryColor
            currentCustomization.idScanCustomization.reviewScreenTextBackgroundBorderColor = primaryColor
            currentCustomization.idScanCustomization.reviewScreenTextBackgroundBorderWidth = 0
            currentCustomization.idScanCustomization.reviewScreenTextBackgroundCornerRadius = 2
            currentCustomization.idScanCustomization.captureScreenBackgroundColor = backgroundColor
            currentCustomization.idScanCustomization.captureFrameStrokeColor = primaryColor
            currentCustomization.idScanCustomization.captureFrameStrokeWith = 2
            currentCustomization.idScanCustomization.captureFrameCornerRadius = 12
            currentCustomization.idScanCustomization.activeTorchButtonImage = UIImage(named: "zoom_active_torch")
            currentCustomization.idScanCustomization.inactiveTorchButtonImage = UIImage(named: "zoom_inactive_torch")
            // Result Screen Customization
            currentCustomization.resultScreenCustomization.backgroundColors = [backgroundColor, backgroundColor]
            currentCustomization.resultScreenCustomization.foregroundColor = secondaryColor
            currentCustomization.resultScreenCustomization.messageFont = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.init(0.3))
            currentCustomization.resultScreenCustomization.messageTextSpacing = 1.5
            currentCustomization.resultScreenCustomization.activityIndicatorColor = primaryColor
            currentCustomization.resultScreenCustomization.customActivityIndicatorImage = UIImage(named: "activity_indicator_red")
            currentCustomization.resultScreenCustomization.customActivityIndicatorRotationInterval = 1500
            currentCustomization.resultScreenCustomization.resultAnimationBackgroundColor = UIColor.clear
            currentCustomization.resultScreenCustomization.resultAnimationForegroundColor = UIColor.clear
            currentCustomization.resultScreenCustomization.resultAnimationSuccessBackgroundImage = nil
            currentCustomization.resultScreenCustomization.resultAnimationUnsuccessBackgroundImage = nil
            currentCustomization.resultScreenCustomization.showUploadProgressBar = false
            currentCustomization.resultScreenCustomization.uploadProgressTrackColor = UIColor.black.withAlphaComponent(0.2)
            currentCustomization.resultScreenCustomization.uploadProgressFillColor = primaryColor
            currentCustomization.resultScreenCustomization.animationRelativeScale = 1.0
            // Feedback Customization
            currentCustomization.feedbackCustomization.backgroundColor = backgroundLayer
            currentCustomization.feedbackCustomization.textColor = backgroundColor
            currentCustomization.feedbackCustomization.textFont = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.init(0.3))
            currentCustomization.feedbackCustomization.textSpacing = 1.5
            currentCustomization.feedbackCustomization.cornerRadius = 3
            currentCustomization.feedbackCustomization.shadow = zoomFeedbackShadow
            currentCustomization.feedbackCustomization.relativeWidth = 1.0
            // Frame Customization
            currentCustomization.frameCustomization.backgroundColor = backgroundColor
            currentCustomization.frameCustomization.borderColor = primaryColor
            currentCustomization.frameCustomization.borderWidth = 2
            currentCustomization.frameCustomization.cornerRadius = 8
            currentCustomization.frameCustomization.shadow = zoomFrameShadow
            // Oval Customization
            currentCustomization.ovalCustomization.strokeColor = primaryColor
            currentCustomization.ovalCustomization.progressColor1 = primaryColor.withAlphaComponent(0.5)
            currentCustomization.ovalCustomization.progressColor2 = primaryColor.withAlphaComponent(0.5)
            // Cancel Button Customization
            currentCustomization.cancelButtonCustomization.customImage = UIImage(named: "cancel_box_red")
            currentCustomization.cancelButtonCustomization.location = ZoomCancelButtonLocation.topRight
        }
        else if theme == "Sample Bank" {
            let primaryColor = UIColor.white
            let backgroundColor = UIColor(red: 0.114, green: 0.090, blue: 0.310, alpha: 1) // navy
            let backgroundLayer = CAGradientLayer.init()
            backgroundLayer.colors = [primaryColor.cgColor, primaryColor.cgColor]
            backgroundLayer.locations = [0,1]
            backgroundLayer.startPoint = CGPoint.init(x: 0, y: 0)
            backgroundLayer.endPoint = CGPoint.init(x: 1, y: 0)
            
            let zoomFeedbackShadow: ZoomShadow? = nil
            let zoomFrameShadow: ZoomShadow? = nil

            // Overlay Customization
            currentCustomization.overlayCustomization.blurEffectStyle = ZoomBlurEffectStyle.off
            currentCustomization.overlayCustomization.blurEffectOpacity = 0
            currentCustomization.overlayCustomization.backgroundColor = UIColor.clear
            currentCustomization.overlayCustomization.showBrandingImage = true
            currentCustomization.overlayCustomization.brandingImage = UIImage(named: "sample_bank_logo")
            // Guidance Customization
            currentCustomization.guidanceCustomization.backgroundColors = [backgroundColor, backgroundColor]
            currentCustomization.guidanceCustomization.foregroundColor = primaryColor
            currentCustomization.guidanceCustomization.headerFont = UIFont.systemFont(ofSize: 26, weight: UIFont.Weight.init(0.3))
            currentCustomization.guidanceCustomization.headerTextSpacing = 1.5
            currentCustomization.guidanceCustomization.subtextFont = UIFont.systemFont(ofSize: 16, weight: .light)
            currentCustomization.guidanceCustomization.subtextTextSpacing = 0
            currentCustomization.guidanceCustomization.buttonFont = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.init(0.4))
            currentCustomization.guidanceCustomization.buttonTextSpacing = 1.5
            currentCustomization.guidanceCustomization.buttonTextNormalColor = backgroundColor
            currentCustomization.guidanceCustomization.buttonBackgroundNormalColor = primaryColor
            currentCustomization.guidanceCustomization.buttonTextHighlightColor = backgroundColor
            currentCustomization.guidanceCustomization.buttonBackgroundHighlightColor = primaryColor.withAlphaComponent(0.8)
            currentCustomization.guidanceCustomization.buttonTextDisabledColor = backgroundColor.withAlphaComponent(0.3)
            currentCustomization.guidanceCustomization.buttonBackgroundDisabledColor = primaryColor
            currentCustomization.guidanceCustomization.buttonBorderColor = primaryColor
            currentCustomization.guidanceCustomization.buttonBorderWidth = 2
            currentCustomization.guidanceCustomization.buttonCornerRadius = 2
            currentCustomization.guidanceCustomization.buttonRelativeWidth = 1.0
            currentCustomization.guidanceCustomization.readyScreenOvalFillColor = primaryColor.withAlphaComponent(0.2)
            currentCustomization.guidanceCustomization.readyScreenTextBackgroundColor = backgroundColor
            currentCustomization.guidanceCustomization.readyScreenTextBackgroundCornerRadius = 2
            currentCustomization.guidanceCustomization.retryScreenImageBorderColor = primaryColor
            currentCustomization.guidanceCustomization.retryScreenImageBorderWidth = 2
            currentCustomization.guidanceCustomization.retryScreenImageCornerRadius = 2
            currentCustomization.guidanceCustomization.retryScreenOvalStrokeColor = primaryColor
            currentCustomization.guidanceCustomization.retryScreenSlideshowImages = retryScreenSlideshowImages
            currentCustomization.guidanceCustomization.retryScreenSlideshowInterval = 1500
            currentCustomization.guidanceCustomization.enableRetryScreenSlideshowShuffle = false
            currentCustomization.guidanceCustomization.enableRetryScreenBulletedInstructions = false
            currentCustomization.guidanceCustomization.cameraPermissionsScreenImage = UIImage(named: "camera_white_navy")
            // ID Scan Customization
            currentCustomization.idScanCustomization.showSelectionScreenBrandingImage = false
            currentCustomization.idScanCustomization.selectionScreenBrandingImage = nil
            currentCustomization.idScanCustomization.selectionScreenBackgroundColors = [backgroundColor, backgroundColor]
            currentCustomization.idScanCustomization.reviewScreenBackgroundColors = [backgroundColor, backgroundColor]
            currentCustomization.idScanCustomization.captureScreenForegroundColor = backgroundColor
            currentCustomization.idScanCustomization.reviewScreenForegroundColor = backgroundColor
            currentCustomization.idScanCustomization.selectionScreenForegroundColor = primaryColor
            currentCustomization.idScanCustomization.captureScreenFocusMessageTextColor = primaryColor
            currentCustomization.idScanCustomization.headerFont = UIFont.systemFont(ofSize: 26, weight: UIFont.Weight.init(0.3))
            currentCustomization.idScanCustomization.headerTextSpacing = 1.5
            currentCustomization.idScanCustomization.subtextFont = UIFont.systemFont(ofSize: 16, weight: .light)
            currentCustomization.idScanCustomization.subtextTextSpacing = 0
            currentCustomization.idScanCustomization.buttonFont = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.init(0.4))
            currentCustomization.idScanCustomization.buttonTextSpacing = 1.5
            currentCustomization.idScanCustomization.buttonTextNormalColor = backgroundColor
            currentCustomization.idScanCustomization.buttonBackgroundNormalColor = primaryColor
            currentCustomization.idScanCustomization.buttonTextHighlightColor = backgroundColor
            currentCustomization.idScanCustomization.buttonBackgroundHighlightColor = primaryColor.withAlphaComponent(0.8)
            currentCustomization.idScanCustomization.buttonTextDisabledColor = backgroundColor.withAlphaComponent(0.3)
            currentCustomization.idScanCustomization.buttonBackgroundDisabledColor = primaryColor
            currentCustomization.idScanCustomization.buttonBorderColor = primaryColor
            currentCustomization.idScanCustomization.buttonBorderWidth = 2
            currentCustomization.idScanCustomization.buttonCornerRadius = 2
            currentCustomization.idScanCustomization.buttonRelativeWidth = 1.0
            currentCustomization.idScanCustomization.captureScreenTextBackgroundColor = primaryColor
            currentCustomization.idScanCustomization.captureScreenTextBackgroundBorderColor = backgroundColor
            currentCustomization.idScanCustomization.captureScreenTextBackgroundBorderWidth = 2
            currentCustomization.idScanCustomization.captureScreenTextBackgroundCornerRadius = 2
            currentCustomization.idScanCustomization.reviewScreenTextBackgroundColor = primaryColor
            currentCustomization.idScanCustomization.reviewScreenTextBackgroundBorderColor = backgroundColor
            currentCustomization.idScanCustomization.reviewScreenTextBackgroundBorderWidth = 2
            currentCustomization.idScanCustomization.reviewScreenTextBackgroundCornerRadius = 2
            currentCustomization.idScanCustomization.captureScreenBackgroundColor = backgroundColor
            currentCustomization.idScanCustomization.captureFrameStrokeColor = primaryColor
            currentCustomization.idScanCustomization.captureFrameStrokeWith = 2
            currentCustomization.idScanCustomization.captureFrameCornerRadius = 12
            currentCustomization.idScanCustomization.activeTorchButtonImage = UIImage(named: "torch_active_white")
            currentCustomization.idScanCustomization.inactiveTorchButtonImage = UIImage(named: "torch_inactive_white")
            // Result Screen Customization
            currentCustomization.resultScreenCustomization.backgroundColors = [backgroundColor, backgroundColor]
            currentCustomization.resultScreenCustomization.foregroundColor = primaryColor
            currentCustomization.resultScreenCustomization.messageFont = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.init(0.3))
            currentCustomization.resultScreenCustomization.messageTextSpacing = 1.5
            currentCustomization.resultScreenCustomization.activityIndicatorColor = primaryColor
            currentCustomization.resultScreenCustomization.customActivityIndicatorImage = UIImage(named: "activity_indicator_white")
            currentCustomization.resultScreenCustomization.customActivityIndicatorRotationInterval = 1000
            currentCustomization.resultScreenCustomization.resultAnimationBackgroundColor = UIColor.clear
            currentCustomization.resultScreenCustomization.resultAnimationForegroundColor = primaryColor
            currentCustomization.resultScreenCustomization.resultAnimationSuccessBackgroundImage = UIImage(named: "reticle_white")
            currentCustomization.resultScreenCustomization.resultAnimationUnsuccessBackgroundImage = UIImage(named: "reticle_white")
            currentCustomization.resultScreenCustomization.showUploadProgressBar = true
            currentCustomization.resultScreenCustomization.uploadProgressTrackColor = UIColor.white.withAlphaComponent(0.2)
            currentCustomization.resultScreenCustomization.uploadProgressFillColor = primaryColor
            currentCustomization.resultScreenCustomization.animationRelativeScale = 1.0
            // Feedback Customization
            currentCustomization.feedbackCustomization.backgroundColor = backgroundLayer
            currentCustomization.feedbackCustomization.textColor = backgroundColor
            currentCustomization.feedbackCustomization.textFont = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.init(0.3))
            currentCustomization.feedbackCustomization.textSpacing = 1.5
            currentCustomization.feedbackCustomization.cornerRadius = 2
            currentCustomization.feedbackCustomization.shadow = zoomFeedbackShadow
            currentCustomization.feedbackCustomization.relativeWidth = 1.0
            // Frame Customization
            currentCustomization.frameCustomization.backgroundColor = backgroundColor
            currentCustomization.frameCustomization.borderColor = primaryColor
            currentCustomization.frameCustomization.borderWidth = 2
            currentCustomization.frameCustomization.cornerRadius = 2
            currentCustomization.frameCustomization.shadow = zoomFrameShadow
            // Oval Customization
            currentCustomization.ovalCustomization.strokeColor = primaryColor
            currentCustomization.ovalCustomization.progressColor1 = primaryColor.withAlphaComponent(0.5)
            currentCustomization.ovalCustomization.progressColor2 = primaryColor.withAlphaComponent(0.5)
            // Cancel Button Customization
            currentCustomization.cancelButtonCustomization.customImage = UIImage(named: "cancel_white")
            currentCustomization.cancelButtonCustomization.location = ZoomCancelButtonLocation.topLeft
        } else {
            currentCustomization.overlayCustomization.showBrandingImage = false
        }
        return currentCustomization
    }
    
    // Configure UX Color Scheme For Low Light Mode
    class func getLowLightCustomizationForTheme(theme: String) -> ZoomCustomization? {

        var currentLowLightCustomization: ZoomCustomization? = getCustomizationForTheme(theme: theme)
        
        let retryScreenSlideshowImages = [UIImage(named: "zoom_ideal_1")!, UIImage(named: "zoom_ideal_2")!, UIImage(named: "zoom_ideal_3")!, UIImage(named: "zoom_ideal_4")!, UIImage(named: "zoom_ideal_5")!]

        if theme == "FaceTec Theme" {
            currentLowLightCustomization = nil
        }
        else if theme == "Well-Rounded" {
            currentLowLightCustomization = nil
        }
        else if theme == "Bitcoin Exchange" {
            let primaryColor = UIColor(red: 0.969, green: 0.588, blue: 0.204, alpha: 1) // orange
            let secondaryColor = UIColor(red: 1, green: 1, blue: 0.188, alpha: 1) // yellow
            let backgroundColor = UIColor(red: 0.259, green: 0.259, blue: 0.259, alpha: 1) // dark grey
            let backgroundLayer = CAGradientLayer.init()
            backgroundLayer.colors = [backgroundColor.cgColor, backgroundColor.cgColor]
            backgroundLayer.locations = [0,1]
            backgroundLayer.startPoint = CGPoint.init(x: 0, y: 0)
            backgroundLayer.endPoint = CGPoint.init(x: 1, y: 0)
            
            // Overlay Customization
            currentLowLightCustomization?.overlayCustomization.brandingImage = UIImage(named: "bitcoin_exchange_logo")
            // Guidance Customization
            currentLowLightCustomization?.guidanceCustomization.foregroundColor = backgroundColor
            currentLowLightCustomization?.guidanceCustomization.buttonTextNormalColor = UIColor.white
            currentLowLightCustomization?.guidanceCustomization.buttonBackgroundNormalColor = primaryColor
            currentLowLightCustomization?.guidanceCustomization.buttonTextHighlightColor = UIColor.white
            currentLowLightCustomization?.guidanceCustomization.buttonBackgroundHighlightColor = primaryColor
            currentLowLightCustomization?.guidanceCustomization.buttonTextDisabledColor = UIColor.white
            currentLowLightCustomization?.guidanceCustomization.buttonBackgroundDisabledColor = primaryColor
            currentLowLightCustomization?.guidanceCustomization.buttonBorderColor = UIColor.clear
            currentLowLightCustomization?.guidanceCustomization.readyScreenOvalFillColor = primaryColor.withAlphaComponent(0.2)
            currentLowLightCustomization?.guidanceCustomization.readyScreenTextBackgroundColor = primaryColor
            currentLowLightCustomization?.guidanceCustomization.retryScreenImageBorderColor = primaryColor
            currentLowLightCustomization?.guidanceCustomization.retryScreenOvalStrokeColor = primaryColor
            currentLowLightCustomization?.guidanceCustomization.retryScreenSlideshowImages = []
            // ID Scan Customization
            currentLowLightCustomization?.idScanCustomization.selectionScreenBrandingImage = nil
            currentLowLightCustomization?.idScanCustomization.captureScreenForegroundColor = UIColor.white
            currentLowLightCustomization?.idScanCustomization.reviewScreenForegroundColor = UIColor.white
            currentLowLightCustomization?.idScanCustomization.selectionScreenForegroundColor = backgroundColor
            currentLowLightCustomization?.idScanCustomization.captureScreenFocusMessageTextColor = backgroundColor
            currentLowLightCustomization?.idScanCustomization.buttonTextNormalColor = UIColor.white
            currentLowLightCustomization?.idScanCustomization.buttonBackgroundNormalColor = primaryColor
            currentLowLightCustomization?.idScanCustomization.buttonTextHighlightColor = UIColor.white
            currentLowLightCustomization?.idScanCustomization.buttonBackgroundHighlightColor = primaryColor.withAlphaComponent(0.8)
            currentLowLightCustomization?.idScanCustomization.buttonTextDisabledColor = UIColor.white
            currentLowLightCustomization?.idScanCustomization.buttonBackgroundDisabledColor = primaryColor
            currentLowLightCustomization?.idScanCustomization.buttonBorderColor = UIColor.clear
            currentLowLightCustomization?.idScanCustomization.captureScreenTextBackgroundColor = backgroundColor
            currentLowLightCustomization?.idScanCustomization.captureScreenTextBackgroundBorderColor = UIColor.clear
            currentLowLightCustomization?.idScanCustomization.reviewScreenTextBackgroundColor = backgroundColor
            currentLowLightCustomization?.idScanCustomization.reviewScreenTextBackgroundBorderColor = UIColor.clear
            currentLowLightCustomization?.idScanCustomization.captureFrameStrokeColor = primaryColor
            currentLowLightCustomization?.idScanCustomization.activeTorchButtonImage = UIImage(named: "torch_active_orange")
            currentLowLightCustomization?.idScanCustomization.inactiveTorchButtonImage = UIImage(named: "torch_inactive_orange")
            // Result Screen Customization
            currentLowLightCustomization?.resultScreenCustomization.foregroundColor = backgroundColor
            currentLowLightCustomization?.resultScreenCustomization.activityIndicatorColor = primaryColor
            currentLowLightCustomization?.resultScreenCustomization.customActivityIndicatorImage = UIImage(named: "activity_indicator_orange")
            currentLowLightCustomization?.resultScreenCustomization.resultAnimationBackgroundColor = primaryColor
            currentLowLightCustomization?.resultScreenCustomization.resultAnimationForegroundColor = UIColor.white
            currentLowLightCustomization?.resultScreenCustomization.resultAnimationSuccessBackgroundImage = nil
            currentLowLightCustomization?.resultScreenCustomization.resultAnimationUnsuccessBackgroundImage = nil
            currentLowLightCustomization?.resultScreenCustomization.uploadProgressTrackColor = UIColor.black.withAlphaComponent(0.2)
            currentLowLightCustomization?.resultScreenCustomization.uploadProgressFillColor = primaryColor
            // Feedback Customization
            currentLowLightCustomization?.feedbackCustomization.backgroundColor = backgroundLayer
            currentLowLightCustomization?.feedbackCustomization.textColor = UIColor.white
            // Frame Customization
            currentLowLightCustomization?.frameCustomization.borderColor = backgroundColor
            // Oval Customization
            currentLowLightCustomization?.ovalCustomization.strokeColor = primaryColor
            currentLowLightCustomization?.ovalCustomization.progressColor1 = secondaryColor
            currentLowLightCustomization?.ovalCustomization.progressColor2 = secondaryColor
            // Cancel Button Customization
            currentLowLightCustomization?.cancelButtonCustomization.customImage = UIImage(named: "single_chevron_left_orange")
            
            // Guidance Customization -- Text Style Overrides
            // Ready Screen Header
            currentLowLightCustomization?.guidanceCustomization.readyScreenHeaderTextColor = primaryColor
            // Ready Screen Subtext
            currentLowLightCustomization?.guidanceCustomization.readyScreenSubtextTextColor = UIColor(red: 0.337, green: 0.337, blue: 0.337, alpha: 1)
            // Retry Screen Header
            currentLowLightCustomization?.guidanceCustomization.retryScreenHeaderTextColor = primaryColor
            // Retry Screen Subtext
            currentLowLightCustomization?.guidanceCustomization.retryScreenSubtextTextColor = UIColor(red: 0.337, green: 0.337, blue: 0.337, alpha: 1)
        }
        else if theme == "eKYC" {
            currentLowLightCustomization = nil
        }
        else if theme == "Sample Bank" {
            let primaryColor = UIColor.white
            let backgroundColor = UIColor(red: 0.114, green: 0.090, blue: 0.310, alpha: 1) // navy
            let backgroundLayer = CAGradientLayer.init()
            backgroundLayer.colors = [backgroundColor.cgColor, backgroundColor.cgColor]
            backgroundLayer.locations = [0,1]
            backgroundLayer.startPoint = CGPoint.init(x: 0, y: 0)
            backgroundLayer.endPoint = CGPoint.init(x: 1, y: 0)

            // Overlay Customization
            currentLowLightCustomization?.overlayCustomization.brandingImage = UIImage(named: "sample_bank_logo")
            // Guidance Customization
            currentLowLightCustomization?.guidanceCustomization.foregroundColor = backgroundColor
            currentLowLightCustomization?.guidanceCustomization.buttonTextNormalColor = primaryColor
            currentLowLightCustomization?.guidanceCustomization.buttonBackgroundNormalColor = backgroundColor
            currentLowLightCustomization?.guidanceCustomization.buttonTextHighlightColor = primaryColor
            currentLowLightCustomization?.guidanceCustomization.buttonBackgroundHighlightColor = backgroundColor.withAlphaComponent(0.8)
            currentLowLightCustomization?.guidanceCustomization.buttonTextDisabledColor = primaryColor.withAlphaComponent(0.3)
            currentLowLightCustomization?.guidanceCustomization.buttonBackgroundDisabledColor = backgroundColor
            currentLowLightCustomization?.guidanceCustomization.buttonBorderColor = backgroundColor
            currentLowLightCustomization?.guidanceCustomization.readyScreenOvalFillColor = backgroundColor.withAlphaComponent(0.2)
            currentLowLightCustomization?.guidanceCustomization.readyScreenTextBackgroundColor = primaryColor
            currentLowLightCustomization?.guidanceCustomization.retryScreenImageBorderColor = backgroundColor
            currentLowLightCustomization?.guidanceCustomization.retryScreenOvalStrokeColor = primaryColor
            currentLowLightCustomization?.guidanceCustomization.retryScreenSlideshowImages = retryScreenSlideshowImages
            // ID Scan Customization
            currentLowLightCustomization?.idScanCustomization.selectionScreenBrandingImage = nil
            currentLowLightCustomization?.idScanCustomization.captureScreenForegroundColor = backgroundColor
            currentLowLightCustomization?.idScanCustomization.reviewScreenForegroundColor = backgroundColor
            currentLowLightCustomization?.idScanCustomization.selectionScreenForegroundColor = backgroundColor
            currentLowLightCustomization?.idScanCustomization.captureScreenFocusMessageTextColor = backgroundColor
            currentLowLightCustomization?.idScanCustomization.buttonTextNormalColor = primaryColor
            currentLowLightCustomization?.idScanCustomization.buttonBackgroundNormalColor = backgroundColor
            currentLowLightCustomization?.idScanCustomization.buttonTextHighlightColor = primaryColor
            currentLowLightCustomization?.idScanCustomization.buttonBackgroundHighlightColor = backgroundColor.withAlphaComponent(0.8)
            currentLowLightCustomization?.idScanCustomization.buttonTextDisabledColor = primaryColor.withAlphaComponent(0.3)
            currentLowLightCustomization?.idScanCustomization.buttonBackgroundDisabledColor = backgroundColor
            currentLowLightCustomization?.idScanCustomization.buttonBorderColor = backgroundColor
            currentLowLightCustomization?.idScanCustomization.captureScreenTextBackgroundColor = primaryColor
            currentLowLightCustomization?.idScanCustomization.captureScreenTextBackgroundBorderColor = backgroundColor
            currentLowLightCustomization?.idScanCustomization.reviewScreenTextBackgroundColor = primaryColor
            currentLowLightCustomization?.idScanCustomization.reviewScreenTextBackgroundBorderColor = backgroundColor
            currentLowLightCustomization?.idScanCustomization.captureFrameStrokeColor = primaryColor
            currentLowLightCustomization?.idScanCustomization.activeTorchButtonImage = UIImage(named: "torch_active_navy")
            currentLowLightCustomization?.idScanCustomization.inactiveTorchButtonImage = UIImage(named: "torch_inactive_navy")
            // Result Screen Customization
            currentLowLightCustomization?.resultScreenCustomization.foregroundColor = backgroundColor
            currentLowLightCustomization?.resultScreenCustomization.activityIndicatorColor = backgroundColor
            currentLowLightCustomization?.resultScreenCustomization.customActivityIndicatorImage = UIImage(named: "activity_indicator_navy")
            currentLowLightCustomization?.resultScreenCustomization.resultAnimationBackgroundColor = UIColor.clear
            currentLowLightCustomization?.resultScreenCustomization.resultAnimationForegroundColor = backgroundColor
            currentLowLightCustomization?.resultScreenCustomization.resultAnimationSuccessBackgroundImage = UIImage(named: "reticle_navy")
            currentLowLightCustomization?.resultScreenCustomization.resultAnimationUnsuccessBackgroundImage = UIImage(named: "reticle_navy")
            currentLowLightCustomization?.resultScreenCustomization.uploadProgressTrackColor = UIColor.black.withAlphaComponent(0.2)
            currentLowLightCustomization?.resultScreenCustomization.uploadProgressFillColor = backgroundColor
            // Feedback Customization
            currentLowLightCustomization?.feedbackCustomization.backgroundColor = backgroundLayer
            currentLowLightCustomization?.feedbackCustomization.textColor = primaryColor
            // Frame Customization
            currentLowLightCustomization?.frameCustomization.borderColor = backgroundColor
            // Oval Customization
            currentLowLightCustomization?.ovalCustomization.strokeColor = backgroundColor
            currentLowLightCustomization?.ovalCustomization.progressColor1 = backgroundColor.withAlphaComponent(0.5)
            currentLowLightCustomization?.ovalCustomization.progressColor2 = backgroundColor.withAlphaComponent(0.5)
            // Cancel Button Customization
            currentLowLightCustomization?.cancelButtonCustomization.customImage = UIImage(named: "cancel_navy")
        }
        else if theme == "Pseudo-Fullscreen" {
            currentLowLightCustomization = nil
        }
        
        return currentLowLightCustomization
    }
}

