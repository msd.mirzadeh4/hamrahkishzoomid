//
//  ResponseIndividualAndImage.swift
//  Cheetah
//
//  Created by Erfan Saadatmand on 7/8/20.
//  Copyright © 2020 GSSInt. All rights reserved.
//

import Foundation

class ResponsePostInquiry {

    var statusCode: Int!
    var message: String!
    var refreshToken: String!
    var accessToken: String!
    
    //MARK- Setter -
    func setRefreshToken(refreshToken:String){
        self.refreshToken = refreshToken
    }
    func setAccessToken(accessToken: String){
        self.accessToken = accessToken
    }
    func setStatusCode(statusCode: Int){
        self.statusCode = statusCode
    }
    
    func setMessage(message: String){
        self.message = message
    }
    
    //MARK- Getter -
    func getRefreshToken()-> String{
        return self.refreshToken
    }
    func getAccessToken()-> String{
        return self.accessToken
    }
    func getMessage()-> String {
        return self.message
    }
    
    func getStatusCode()-> Int {
        return self.statusCode
    }

}
