//
//  ResponseCityCodeDetail.swift
//  Cheetah
//
//  Created by Mojtaba Mirzadeh on 6/24/1399 AP.
//  Copyright © 1399 GSSInt. All rights reserved.
//

import Foundation

class ResponseCityCodeDetail {
    
    var id: Int!
    var createdBy: String!
    var createDate: String!
    var name: String!
    var code: Int!
    var stateCode: Int!
    
    init(id: Int, name: String, code: Int, stateCode: Int){
        self.id = id
        self.name = name
        self.code = code
        self.stateCode = stateCode
        
        self.createdBy = nil
        self.createDate = nil
    }
    
    //MARK- Getter -
    func getId()-> Int{
        return self.id
    }
    
    func getName()-> String{
        return self.name
    }
    
    func getCode()-> Int{
        return self.code
    }
    
    func getStateCode()-> Int {
        return self.stateCode
    }
}
