//
//  LifeState.swift
//  Cheetah
//
//  Created by Erfan Saadatmand on 7/8/20.
//  Copyright © 2020 GSSInt. All rights reserved.
//

import Foundation

class LifeState {
    
    var title: String!
    var code: String!
    
    init(title: String, code: String ){
      self.title = title
      self.code = code
    }
    
    //MARK- Getter -
    func getTitle()-> String{
        return self.title
    }
    
    func getCode()-> String{
        return self.code
    }
}
