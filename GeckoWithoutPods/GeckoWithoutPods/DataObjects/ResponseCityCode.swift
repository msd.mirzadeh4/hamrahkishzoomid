//
//  ResponseCityCode.swift
//  Cheetah
//
//  Created by Mojtaba Mirzadeh on 6/24/1399 AP.
//  Copyright © 1399 GSSInt. All rights reserved.
//

import Foundation

class ResponseCityCode {

//    var statusCode: Int!
//    var message: String!
//    var result = [ResponseCityCodeDetail]()
//
//    init(statusCode: Int, message: String){
//        self.statusCode = statusCode
//        self.message = message
//    }
//
//    ///  Setter
//    func setResult(result: [ResponseCityCodeDetail]){
//        self.result = result
//    }
//
//    //MARK - Gtter -
//    func getStatusCode()-> Int{
//        return self.statusCode
//    }
//    func getMessage()-> String{
//        return self.message
//    }
//    func getResult()->[ResponseCityCodeDetail] {
//        return self.result
//    }
//
//}

    private var id: Int!
    private var cityName: String!
    private var cityCode: Int!
    private var stateCode: Int!
        
    //MARK - initialize
    
    init(){}
    
    init(cityCode: Int ,cityName: String, stateCode: Int){

        self.cityCode = cityCode
        self.cityName = cityName
        self.stateCode = stateCode
    }
    
    //MARK - Getter
    func getId()-> Int{
        return self.id
    }
    func getCityCode()-> Int{
        return self.cityCode
    }
    func getCityName()-> String{
        return self.cityName
    }
    func getStateCode()-> Int{
        return self.stateCode
    }

    
    //MARK - Setter
    
    func setId(id:Int){
        self.id = id
    }
    func setCityCode(cityCode: Int){
        self.cityCode = cityCode
    }
    func setCityName(cityName: String){
        self.cityName = cityName
    }
    func setStateCode(stateCode: Int){
        self.stateCode = stateCode
    }

}
