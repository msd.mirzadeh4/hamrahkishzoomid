//
//  ResponseIndividualAndImage.swift
//  Cheetah
//
//  Created by Mojtaba Mirzadeh on 6/10/1399 AP.
//  Copyright © 1399 GSSInt. All rights reserved.
//

import Foundation

class ResponseIndividualAndImage {
    
    var statusCode: Int!
    var message: String!
    var certificateSeries: String!
    var lastName: String!
    var nationalCode: String!
    var fatherName: String!
    var birthDate: String!
    var certificateNumber: String!
    var lifeState: LifeState!
    var firstName: String!
    var certificateSerial: String!
    var deathDate: String!
    var gender: GenderUser!
    var personImage: String!
    var stateCode: Int!
    var cityCode: Int!
    var address: String!
    var postalCode: String!
    var status: String!
    var englishFirstName: String!
    var englishLastName: String!
    
    //MARK- Setter -
    func setStatusCode(statusCode: Int){
        self.statusCode = statusCode
    }
    
    func setMessage(message: String){
        self.message = message
    }
    
    func setCertificateSeries(certificateSeries: String){
        self.certificateSeries = certificateSeries
    }
    
    func setLastName(lastName: String){
        self.lastName = lastName
    }

    func setNationalCode(nationalCode: String){
        self.nationalCode = nationalCode
    }

    func setFatherName(fatherName: String){
        self.fatherName = fatherName
    }

    func setBirthDate(birthDate: String) {
        self.birthDate = birthDate
    }

    func setCertificateNumber(certificateNumber: String) {
        self.certificateNumber = certificateNumber
    }

    func setLifeState(lifeState: LifeState) {
        self.lifeState = lifeState
    }

    func setFirstName(firstName: String) {
        self.firstName = firstName
    }

    func setSertificateSerial(certificateSerial: String) {
        self.certificateSerial = certificateSerial
    }

    func setDeathDate(deathDate: String) {
        self.deathDate = deathDate
    }

    func setGender(gender: GenderUser) {
        self.gender = gender
    }

    func setPersonImage(personImage: String){
        self.personImage = personImage
    }

    func setStateCode(stateCode: Int) {
        self.stateCode = stateCode
    }

    func setCityCode(cityCode: Int) {
        self.cityCode = cityCode
    }

    func setAdress(address: String) {
        self.address = address
    }

    func setPostalCode(postalCode: String) {
        self.postalCode = postalCode
    }

    func setStatus(status: String) {
        self.status = status
    }
    
    func setEnglishFirstName(englishFirstName: String) {
        self.englishFirstName = englishFirstName
    }
    
    func setEnglishLastName(englishLastName: String) {
        self.englishLastName = englishLastName
    }
    
    //MARK- Getter -
    func getCertificateSeries()-> String{
        return self.certificateSeries
    }

    func getLastName()-> String{
        return self.lastName
    }

    func getNationalCode()-> String{
        return self.nationalCode
    }

    func getFatherName()-> String{
        return self.fatherName
    }

    func getBirthDate()-> String {
        return self.birthDate
    }

    func getCertificateNumber()-> String {
        return self.certificateNumber
    }

    func getLifeState()-> LifeState {
        return self.lifeState
    }

    func getFirstName()-> String {
        return self.firstName
    }

    func getSertificateSerial()-> String {
        return self.certificateSerial
    }

    func getDeathDate()-> String {
        return self.deathDate
    }

    func getGender()-> GenderUser {
        return self.gender
    }

    func getPersonImage()-> String{
        return self.personImage
    }

    func getStateCode() -> Int{
        return self.stateCode
    }

    func getCityCode()-> Int {
        return self.cityCode
    }

    func getAdress() -> String{
        return self.address
    }

    func getPostalCode()-> String {
        return self.postalCode
    }

    func getStatus()-> String {
        return self.status
    }
    
    func getEnglishLastName()-> String {
        return self.englishLastName
    }
    
    func getEnglishFirstName()-> String {
        return self.englishFirstName
    }
    
    func getMessage()-> String {
        return self.message
    }
    
    func getStatusCode()-> Int {
        return self.statusCode
    }
}
