//
//  ResponseStateCode.swift
//  Cheetah
//
//  Created by Mojtaba Mirzadeh on 6/24/1399 AP.
//  Copyright © 1399 GSSInt. All rights reserved.
//

import Foundation

class ResponseStateCode {
    
    //    var statusCode: Int!
    //    var message: String!
    //    var result = [ResponseStateCodeDetail]()
    //
    //    init(statusCode: Int, message: String){
    //        self.statusCode = statusCode
    //        self.message = message
    //    }
    //
    //    ///  Setter
    //    func setResult(result: [ResponseStateCodeDetail]){
    //        self.result = result
    //    }
    //
    //    //MARK - Gtter -
    //    func getStatusCode()-> Int{
    //        return self.statusCode
    //    }
    //    func getMessage()-> String{
    //        return self.message
    //    }
    //    func getResult()->[ResponseStateCodeDetail] {
    //        return self.result
    //    }
    //
    //}
    
    private var id: Int!
    private var stateCode: Int!
    private var stateName: String!
    
    //MARK - initialize
    
    init(){
    }
    init(stateCode: Int, stateName: String){
        self.stateCode = stateCode
        self.stateName = stateName
    }
    
    //MARK - Getter
    
    func getId()-> Int{
        return self.id
    }
    func getStateCode()-> Int{
        return self.stateCode
    }
    func getStateName()-> String{
        return self.stateName
    }
    
    //MARK - Setter
    
    func setId(id:Int){
        self.id = id
    }
    func setStateCode(stateCode: Int){
        self.stateCode = stateCode
    }
    func setStateName(stateName: String){
        self.stateName = stateName
    }
}
