//
//  Helper.swift
//  GeckoFramework
//
//  Created by Mojtaba Mirzadeh on 8/25/1399 AP.
//  Copyright © 1399 AP Hamrahkish. All rights reserved.
//

import Foundation

class Helper {
    
    static let shared : Helper = Helper()
    
    //MARK - DateFormating
    func dateFormat(inputStr: String , inputF: String , outputF: String , inputCalendar : Calendar , outputCalendar : Calendar) -> String {
        let insertDate = inputStr
        let firstDateFormatter = DateFormatter()
        firstDateFormatter.dateFormat = inputF
        firstDateFormatter.calendar = inputCalendar
        let outDateFormat = DateFormatter()
        outDateFormat.calendar = outputCalendar
        outDateFormat.dateFormat = outputF
        
        let firstDate = firstDateFormatter.date(from: insertDate)
        let outputDateString = outDateFormat.string(from: firstDate!)
        return outputDateString
    }
    
    enum LoginErrors: Error {
        case badUsername
        case badPassword
    }
    
    //Start getPrivateKeyFromKeychainAndSign
    func getPrivateKeyFromKeychainAndSign(tagPrivate: String, data: CFData) -> String {
        
        let keyAlgorithm = KeyAlgorithm.ec(signatureType: .sha256)
        let tagPrivate = tagPrivate
        
        let getquery: [String: Any] = [kSecClass as String: kSecClassKey,
                                       kSecAttrApplicationTag as String: tagPrivate,
                                       kSecAttrKeyType as String: keyAlgorithm.secKeyAttrType,
                                       kSecReturnRef as String: true]
        
        var item: CFTypeRef?
        let status = SecItemCopyMatching(getquery as CFDictionary, &item)
        xprint(status)
        
        do {
            let key = item as! SecKey
            var error: Unmanaged<CFError>?
            let signature = SecKeyCreateSignature(key, keyAlgorithm.signatureAlgorithm, data, &error) as Data?
            
            return signature!.base64EncodedString()
        } catch {
            xprint("Bad Access")
        }
    }//End getPrivateKeyFromKeychainAndSign
}

// TODO: Fix Response Log
var xprintIndex = 0

enum XprintType: String {
    case error = "🚑"
    case warning = "⚠️"
    case info = "ℹ️"
    case debug = "🔨"
}

func xprint(_ items: Any..., type: XprintType = .debug, file: String = #file, line: Int = #line) {
    
    let fileUrl = URL(string: file)
    let filename = fileUrl?.lastPathComponent ?? ""
    
    print("---[\(type.rawValue) \(xprintIndex).𝙓𝙋𝙍𝙄𝙉𝙏 @ \(filename):\(line) \(type.rawValue)]---")
    for item in items {
        print(item)
    }
    print(String(repeating: "-", count: 66))
    xprintIndex += 1
}
