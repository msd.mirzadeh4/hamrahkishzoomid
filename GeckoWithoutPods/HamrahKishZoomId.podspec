Pod::Spec.new do |spec|

  spec.name         = "HamrahKishZoomId"
  spec.version      = "1.1.0"
  spec.summary      = "This is an module about Zoomid that created from HamrahKish"
  spec.description  = "HamrahKish Company create this module for other companies to use that for authentication. Clients have to get LicenseText From HamrahKish."
  spec.homepage     = "https://gitlab.com/msd.mirzadeh4/hamrahkishzoomid"
  spec.license      = "MIT"
  spec.author       = { "Mojtaba Mirzadeh" => "mojtaba@hamrahkish.com" }
  spec.platform     = :ios, "11.0"
  spec.source       = { :git => "https://gitlab.com/msd.mirzadeh4/hamrahkishzoomid.git", :tag => "1.1.0" }
  spec.source_files  = "GeckoWithoutPods/**/*.{h,m}"

end
